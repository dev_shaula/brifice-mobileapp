const initialState = {
    loading: false,
    error: '',
    profilRisiko: '',
};

const riskProfilReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'RISK_PROFILE_BEGIN':
            return {
                ...state,
                loading: action.loading,
                error: '',
            };
        case 'RISK_PROFILE_FAILED':
            return {
                ...state,
                loading: action.loading,
                error: action.error,
            };
        case 'RISK_PROFILE_SUCCESS':
            return {
                ...state,
                loading: action.loading,
                profilRisiko: action.profilRisiko,
                error: '',
            };
        default:
            return state;
    }
};

export default riskProfilReducer;
