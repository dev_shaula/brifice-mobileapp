import { combineReducers } from 'redux';
import loginReducer from './Auth/loginReducer';
import getUserReducer from './Auth/getUserReducer';
import analyzeReducers from './Produk/analyzeReducers';
import riskProfilReducer from './Akun/riskProfilReducer';
import directInvestReducer from './Produk/directInvestReducer';

const rootReducer = combineReducers({
    auth: loginReducer,
    userProfil: getUserReducer,
    analyze: analyzeReducers,
    riskProfil: riskProfilReducer,
    direct: directInvestReducer,
});

export default rootReducer;
