const initialState = {
    direct: [],
    loading: false,
    error: '',
    detailDirect: {
        id: 0,
        nama_produk: '',
        jenis_produk: '',
        deskripsi_produk: '',
        profil_resiko: 0,
        idle_money: 0,
        total_1: 0,
        total_15: 0,
        total_20: 0,
        total_30: 0,
    },
};

const directInvestReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'DIRECT_INVEST_BEGIN':
            return {
                ...state,
                loading: action.loading,
                error: '',
            };
        case 'DIRECT_INVEST_FAILED':
            return {
                ...state,
                loading: action.loading,
                error: action.error,
            };
        case 'DIRECT_INVEST_SUCCESS':
            return {
                ...state,
                loading: action.loading,
                direct: action.direct,
                error: '',
            };
        case 'DETAIL_DIRECT_INVEST_BEGIN':
            return {
                ...state,
                loading: action.loading,
                error: '',
            };
        case 'DETAIL_DIRECT_INVEST_FAILED':
            return {
                ...state,
                loading: action.loading,
                error: action.error,
            };
        case 'DETAIL_DIRECT_INVEST_SUCCESS':
            return {
                ...state,
                loading: action.loading,
                detailDirect: action.detailDirect,
                error: '',
            };
        default:
            return state;
    }
};

export default directInvestReducer;
