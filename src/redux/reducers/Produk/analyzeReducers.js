const initialState = {
    analisis: '',
    data: [],
    score: 0,
    hasAnswerQuestions: false,
    result: {
        avg_pemasukkan: 0,
        avg_pengeluaran: 0,
        detail: {
            keluar: [],
            masuk: [],
        },
        idle_money: 0,
    },
    loading: false,
    error: '',
    adjustMasuk: [{
        id: 0,
        remarks: '',
        status: false,
    }],
    adjustKeluar: [{
        id: 0,
        remarks: '',
        status: false,
    }],
    loadingAdjust: false,
    opsiAnalyze: [],
    detailOpsiAnalyze: {
        id: 0,
        nama_produk: '',
        jenis_produk: '',
        deskripsi_produk: '',
        profil_resiko: 0,
        idle_money: 0,
        total_1: 0,
        total_15: 0,
        total_20: 0,
        total_30: 0,
    },
};

const analyzeReducers = (state = initialState, action) => {
    switch (action.type) {
        case 'DETAIL_PARAMETER':
            return {
                ...state,
                analisis: action.analisis,
                data: action.data,
                error: '',
            };
        case 'QUESTIONS':
            return {
                ...state,
                score: action.score,
            };
        case 'QUESTIONS_BEGIN':
            return {
                ...state,
                hasAnswerQuestions: true,
                error: '',
            };
        case 'GET_ANALYZE_FAILED':
            return {
                ...state,
                loading: action.loading,
                error: action.error,
            };
        case 'GET_ANALYZE_SUCCESS':
            return {
                ...state,
                loading: action.loading,
                result: action.result,
                error: '',
            };
        case 'EDIT_PARAMETER_BEGIN':
            return {
                ...state,
                loading: action.loading,
                error: '',
            };
        case 'EDIT_PARAMETER_FAILED':
            return {
                ...state,
                loading: action.loading,
                error: action.error,
            };
        case 'EDIT_PARAMETER_SUCCESS':
            return {
                ...state,
                loadingAdjust: action.loadingAdjust,
                error: '',
            };
        case 'GET_ANALYZE_ADJUST_FAILED':
            return {
                ...state,
                loadingAdjust: action.loadingAdjust,
                error: action.error,
            };
        case 'GET_ANALYZE_ADJUST_SUCCESS':
            return {
                ...state,
                loadingAdjust: action.loadingAdjust,
                adjustMasuk: action.adjustMasuk,
                adjustKeluar: action.adjustKeluar,
                analisis: action.analisis,
                error: '',
            };
        case 'GET_ANALYZE_ADJUST_BEGIN':
            return {
                ...state,
                loadingAdjust: action.loadingAdjust,
                error: '',
            };
        case 'OPSI_ANALYZE_FAILED':
            return {
                ...state,
                loading: action.loading,
                error: action.error,
            };
        case 'OPSI_ANALYZE_SUCCESS':
            return {
                ...state,
                loading: action.loading,
                opsiAnalyze: action.opsiAnalyze,
                error: '',
            };
        case 'OPSI_ANALYZE_BEGIN':
            return {
                ...state,
                loading: action.loading,
                error: '',
            };
        case 'DETAIL_OPSI_ANALYZE_FAILED':
            return {
                ...state,
                loading: action.loading,
                error: action.error,
            };
        case 'DETAIL_OPSI_ANALYZE_SUCCESS':
            return {
                ...state,
                loading: action.loading,
                detailOpsiAnalyze: action.detailOpsiAnalyze,
                error: '',
            };
        case 'DETAIL_OPSI_ANALYZE_BEGIN':
            return {
                ...state,
                loading: action.loading,
                error: '',
            };
        default:
            return state;
    }
};

export default analyzeReducers;
