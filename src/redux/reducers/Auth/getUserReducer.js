const initialState = {
    loading: false,
    error: '',
    result: {
        nama: '',
        nomor_rekening: [] || '',
        profil_resiko: '',
    },
};

const getUserReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_USER_BEGIN':
            return {
                ...state,
                loading: action.loading,
                error: '',
            };
        case 'GET_USER_SUCCESS':
            return {
                ...state,
                loading: action.loading,
                result: action.result,
                error: '',
            };
        case 'GET_USER_FAILED':
            return {
                ...state,
                loading: action.loading,
                error: action.error,
            };
        default:
            return state;
    }
};

export default getUserReducer;
