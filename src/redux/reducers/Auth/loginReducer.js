const initialState = {
    hasLogin: false,
    token: '',
    loading: false,
    error: '',
    id: '',
};

const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN_BEGIN':
            return {
                ...state,
                hasLogin: action.hasLogin,
                token: action.token,
                loading: action.loading,
                error: '',
            };
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                hasLogin: action.hasLogin,
                token: action.token,
                loading: action.loading,
                id: action.id,
                error: '',
            };
        case 'LOGIN_FAILED':
            return {
                ...state,
                hasLogin: action.hasLogin,
                token: action.token,
                loading: action.loading,
                error: action.error,
            };
        case 'LOGOUT':
            return {
                ...state,
                hasLogin: action.hasLogin,
                token: action.token,
                loading: action.loading,
                id: action.id,
            };
        default:
            return state;
    }
};

export default loginReducer;
