import signIn from './Auth/actionLogin';
import getUser from './Auth/actionGetUser';
import logout from './Auth/actionLogout';
import { actionDetailParameter, actionQuestions, actionGetAnalyze, actionEditParameter, actionAnalyzeAdjust, actionGetOpsiAnalyze, actionDetailOpsiInvestasi, actionGetOpsiAnalyzeRecommend } from './Produk/actionAnalyze';
import riskProfil from './Akun/actionRiskProfil';
import { actionGetDirectInvest, actionGetDetailDirectInvest, actionGetDetailDirect } from './Produk/actionDirectInvest';

export {
    signIn,
    getUser,
    actionDetailParameter as detailParameter,
    actionQuestions as questions,
    actionGetAnalyze as getAnalyze,
    logout,
    actionEditParameter as editParameter,
    actionAnalyzeAdjust as getAnalyzeAdjust,
    riskProfil,
    actionGetOpsiAnalyze as getOpsiAnalyze,
    actionDetailOpsiInvestasi as getDetailOpsiAnalyze,
    actionGetDirectInvest as getDirectInvest,
    actionGetDetailDirectInvest as getDetailDirectInvest,
    actionGetOpsiAnalyzeRecommend as getOpsiAnalyzeRecommend,
    actionGetDetailDirect as getDetailDirect,
};
