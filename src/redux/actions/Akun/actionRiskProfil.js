import axios from 'axios';

const actionRiskProfil = (token, score, profilRisiko) => {
    return async dispatch => {
        dispatch({
            type: 'RISK_PROFILE_BEGIN',
            loading: true,
        });
        try {
            await axios.post('http://35.186.151.69:32472/user/riskprofile', { nilai: Number(score) }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.status === 'error') {
                        dispatch({
                            type: 'RISK_PROFILE_FAILED',
                            loading: false,
                            error: res.data.status,
                        });
                     }
                    dispatch({
                        type: 'RISK_PROFILE_SUCCESS',
                        loading: false,
                        profilRisiko,
                    });
                })
                .catch(error => {
                    dispatch({
                        type: 'RISK_PROFILE_FAILED',
                        loading: false,
                        error: error.response,
                    });
                });
        } catch (error) {
            dispatch({
                type: 'RISK_PROFILE_FAILED',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};

export default actionRiskProfil;
