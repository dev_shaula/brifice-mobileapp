import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const getUser = (token) => {
    return async dispatch => {
        dispatch({
            type: 'GET_USER_BEGIN',
            loading: true,
        });
        try {
            await axios.get('http://35.186.151.69:32472/user/profile', {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(async res => {
                    if (res.data.status === 'error') {
                        dispatch({
                            type: 'GET_USER_FAILED',
                            loading: false,
                            error: res.data.detail,
                        });
                    }
                    console.log('user yang login', res.data);
                    dispatch({
                        type: 'GET_USER_SUCCESS',
                        loading: false,
                        result: res.data.detail,
                    });
                    await AsyncStorage.setItem('nama', JSON.stringify(res.data.data.nama));
                })
                .catch(err => {
                    dispatch({
                        type: 'GET_USER_FAILED',
                        loading: false,
                        error: err.response.data.error,
                    });
                });
        } catch (error) {
            dispatch({
                type: 'GET_USER_FAILED',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};

export default getUser;
