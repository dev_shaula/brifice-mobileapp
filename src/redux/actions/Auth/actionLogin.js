import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const login = (data, cb) => {
    return async dispatch => {
        dispatch({
            type: 'LOGIN_BEGIN',
            hasLogin: false,
            token: '',
            loading: true,
        });
        try {
            await axios.post('http://35.186.151.69:32472/user/auth', data, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
            .then(async res => {
                if (res.data.status === 'error') {
                    dispatch({
                        type: 'LOGIN_FAILED',
                        hasLogin: false,
                        token: '',
                        loading: false,
                        error: res.data.detail,
                    });
                    return cb(res.data.status, res.data.detail);
                }
                console.log(res.data);
                dispatch({
                    type: 'LOGIN_SUCCESS',
                    hasLogin: true,
                    token: res.data.data.token,
                    loading: false,
                    id: res.data.data.id,
                });
                cb(res.data.status, res.data.detail);
                try {
                    const token = ['token', res.data.data.token];
                    const id = ['id', JSON.stringify(res.data.data.id)];
                    await AsyncStorage.multiSet([token, id]);
                } catch (error) {
                    console.log(error);
                }
            })
            .catch(err => {
                dispatch({
                    type: 'LOGIN_FAILED',
                    hasLogin: false,
                    token: '',
                    loading: false,
                    error:  err.message,
                });
                cb(false, err.message);
                console.log('gagal login', err.message);
            });
        } catch (error) {
            dispatch({
                type: 'LOGIN_FAILED',
                hasLogin: false,
                token: '',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};

export default login;
