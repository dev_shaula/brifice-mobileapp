import AsyncStorage from '@react-native-async-storage/async-storage';

const logout = () => {
    return async dispatch => {
        dispatch({
            type: 'LOGOUT',
            hasLogin: false,
            token: '',
            loading: false,
            id: '',
        });
        await AsyncStorage.removeItem('token');
        await AsyncStorage.removeItem('id');
        await AsyncStorage.removeItem('nama');
    };
};

export default logout;
