import axios from 'axios';

const URL = 'http://35.186.151.69:32472/';

export const actionGetDirectInvest = (token) => {
    return async dispatch => {
        dispatch({
            type: 'DIRECT_INVEST_BEGIN',
            loading: true,
        });
        try {
            await axios.post(`${URL}invest/direct`, null, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
            .then(res => {
                if (res.data.status === 'error') {
                    dispatch({
                        type: 'DIRECT_INVEST_FAILED',
                        loading: false,
                    });
                }
                dispatch({
                    type: 'DIRECT_INVEST_SUCCESS',
                    loading: false,
                    direct: res.data.detail,
                });
            })
            .catch(error => {
                dispatch({
                    type: 'DIRECT_INVEST_FAILED',
                    loading: false,
                    error: error.response.data.error,
                });
            });
        } catch (error) {
            dispatch({
                type: 'DIRECT_INVEST_FAILED',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};

export const actionGetDetailDirectInvest = (token, id) => {
    return async dispatch => {
        dispatch({
            type: 'DETAIL_DIRECT_INVEST_BEGIN',
            loading: true,
        });
        try {
            await axios.post(`${URL}invest/direct/${id}`, null, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
            .then(res => {
                if (res.data.status === 'error') {
                    dispatch({
                        type: 'DETAIL_DIRECT_INVEST_FAILED',
                        loading: false,
                    });
                }
                console.log(res.data.data);
                dispatch({
                    type: 'DETAIL_DIRECT_INVEST_SUCCESS',
                    loading: false,
                    detailDirect: res.data.data,
                });
            })
            .catch(error => {
                dispatch({
                    type: 'DETAIL_DIRECT_INVEST_FAILED',
                    loading: false,
                    error: error.response.data.error,
                });
            });
        } catch (error) {
            dispatch({
                type: 'DETAIL_DIRECT_INVEST_FAILED',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};

export const actionGetDetailDirect = (token, id, idle) => {
    return async dispatch => {
        dispatch({
            type: 'DETAIL_DIRECT_INVEST_BEGIN',
            loading: true,
        });
        try {
            await axios.post(`${URL}invest/analyze/${id}`, { jumlah_investasi: idle }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
            .then(res => {
                if (res.data.status === 'error') {
                    dispatch({
                        type: 'DETAIL_DIRECT_INVEST_FAILED',
                        loading: false,
                    });
                }
                console.log(res.data.data);
                dispatch({
                    type: 'DETAIL_DIRECT_INVEST_SUCCESS',
                    loading: false,
                    detailDirect: res.data.data,
                });
            })
            .catch(error => {
                dispatch({
                    type: 'DETAIL_DIRECT_INVEST_FAILED',
                    loading: false,
                    error: error.response.data.error,
                });
            });
        } catch (error) {
            dispatch({
                type: 'DETAIL_DIRECT_INVEST_FAILED',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};
