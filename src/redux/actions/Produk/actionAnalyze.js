import axios from 'axios';

const URL = 'http://35.186.151.69:32472/';

export const actionDetailParameter = (params) => {
    return dispatch => {
        dispatch({
            type: 'DETAIL_PARAMETER',
            analisis: params.analisis,
            data: params.data,
        });
    };
};

export const actionQuestions = (params) => {
    return dispatch => {
        dispatch({
            type: 'QUESTIONS',
            score: params.score,
        });
    };
};

export const actionGetAnalyze = (token) => {
    return async dispatch => {
        try {
            await axios.post(`${URL}invest/analyze`, null, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.status === 'error') {
                        dispatch({
                            type: 'GET_ANALYZE_FAILED',
                            loading: false,
                            error: res.data.detail,
                        });
                    }
                    dispatch({
                        type: 'GET_ANALYZE_SUCCESS',
                        loading: false,
                        result: res.data,
                    });
                })
                .catch(err => {
                    dispatch({
                        type: 'GET_ANALYZE_FAILED',
                        loading: false,
                        error: err.response.data.error,
                    });
                });
        } catch (error) {
            dispatch({
                type: 'GET_ANALYZE_FAILED',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};

export const actionAnalyzeAdjust = (token, type) => {
    return async dispatch => {
        dispatch({
            type: 'GET_ANALYZE_ADJUST_BEGIN',
            loadingAdjust: true,
        });
        try {
            await axios.get(`${URL}invest/analyze/pre?transaksi=${type}`, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.status === 'error') {
                        dispatch({
                            type: 'GET_ANALYZE_ADJUST_FAILED',
                            loadingAdjust: false,
                            error: res.data.detail,
                        });
                    }
                    dispatch({
                        type: 'GET_ANALYZE_ADJUST_SUCCESS',
                        loadingAdjust: false,
                        adjustMasuk: type === 'masuk' ? res.data : [],
                        adjustKeluar: type === 'keluar' ? res.data : [],
                        analisis: type,
                    });
                })
                .catch(err => {
                    dispatch({
                        type: 'GET_ANALYZE_ADJUST_FAILED',
                        loadingAdjust: false,
                        error: err.response.data.error,
                    });
                });
        } catch (error) {
            dispatch({
                type: 'GET_ANALYZE_ADJUST_FAILED',
                loadingAdjust: false,
                error: 'No Internet Connection',
            });
        }
    };
};

export const actionEditParameter = (token, value, type, navigation) => {
    return async dispatch => {
        dispatch({
            type: 'EDIT_PARAMETER_BEGIN',
            loading: true,
        });
        try {
            await axios.post(`${URL}invest/analyze/adjust/${type}`, value, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.status === 'error') {
                        dispatch({
                            type: 'EDIT_PARAMETER_FAILED',
                            loading: false,
                            error: res.status,
                        });
                    }
                    dispatch({
                        type: 'GET_ANALYZE_SUCCESS',
                        loading: false,
                        result: res.data.detail,
                    });
                    dispatch({
                        type: 'EDIT_PARAMETER_SUCCESS',
                        loading: false,
                    });
                    return navigation;
                })
                .catch(err => {
                    dispatch({
                        type: 'EDIT_PARAMETER_FAILED',
                        loading: false,
                        error: err.response.data.error,
                    });
                });
        } catch (error) {
            dispatch({
                type: 'EDIT_PARAMETER_FAILED',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};

export const actionGetOpsiAnalyze = (token, income, expense, idle, callback) => {
    return async dispatch => {
        dispatch({
            type: 'OPSI_ANALYZE_BEGIN',
            loading: true,
        });
        try {
            await axios.post(`${URL}invest/analyze/post`, { average_income: income, average_expense: expense, idle_money: idle }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.status === 'error') {
                        dispatch({
                            type: 'OPSI_ANALYZE_FAILED',
                            loading: false,
                            error: res.data.status,
                        });
                        callback(res.data.status, res.data.detail);
                    }
                    dispatch({
                        type: 'OPSI_ANALYZE_SUCCESS',
                        loading: false,
                        opsiAnalyze: res.data.detail,
                    });
                    callback(res.data.status, res.data.detail);
                    console.log(income, expense, idle);
                })
                .catch(error => {
                    dispatch({
                        type: 'OPSI_ANALYZE_FAILED',
                        loading: false,
                        error: error.response.data.error,
                    });
                });
        } catch (error) {
            dispatch({
                type: 'OPSI_ANALYZE_FAILED',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};

export const actionDetailOpsiInvestasi = (token, id, idle) => {
    return async dispatch => {
        dispatch({
            type: 'DETAIL_OPSI_ANALYZE_BEGIN',
            loading: true,
        });
        try {
            await axios.post(`${URL}invest/analyze/${id}`, { jumlah_investasi: idle }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.stauts === 'error') {
                        dispatch({
                            type: 'DETAIL_OPSI_ANALYZE_FAILED',
                            loading: false,
                            error: res.data.stauts,
                        });
                    }
                    console.log(idle);
                    dispatch({
                        type: 'DETAIL_OPSI_ANALYZE_SUCCESS',
                        loading: false,
                        detailOpsiAnalyze: res.data.data,
                    });
                })
                .catch(error => {
                    dispatch({
                        type: 'DETAIL_OPSI_ANALYZE_FAILED',
                        loading: false,
                        error: error.response.data.error,
                    });
                });
        } catch (error) {
            dispatch({
                type: 'DETAIL_OPSI_ANALYZE_FAILED',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};

export const actionGetOpsiAnalyzeRecommend = (token) => {
    return async dispatch => {
        dispatch({
            type: 'OPSI_ANALYZE_BEGIN',
            loading: true,
        });
        try {
            await axios.post(`${URL}invest/analyze/continue`, null, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
            })
                .then(res => {
                    if (res.data.status === 'error') {
                        dispatch({
                            type: 'OPSI_ANALYZE_FAILED',
                            loading: false,
                            error: res.data.status,
                        });
                    }
                    dispatch({
                        type: 'OPSI_ANALYZE_SUCCESS',
                        loading: false,
                        opsiAnalyze: res.data.detail,
                    });
                })
                .catch(error => {
                    dispatch({
                        type: 'OPSI_ANALYZE_FAILED',
                        loading: false,
                        error: error.response.data.error,
                    });
                    console.log(error.response.data.error);
                });
        } catch (error) {
            dispatch({
                type: 'OPSI_ANALYZE_FAILED',
                loading: false,
                error: 'No Internet Connection',
            });
        }
    };
};
