import React, { useEffect } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Home, Portofolio, Notifikasi, Profil, Splash, Login, Boarding, Analyze, Direct, EditParameter, OpsiInvestasi, DetailOpsiInvestasi, DetailDirectInvest } from '../pages';
import { BottomNavigator, Questions, ProfilRisiko, SuccessPengajuan } from '../components';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const token = async () => {
    try {
        const hasToken = await AsyncStorage.getItem('token');
        console.log(hasToken !== null ? `hasToken ${hasToken}` : 'no token');
    } catch (error) {
        console.log('router', error);
    }
};

const MainApp = () => {
    return (
        <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
            <Tab.Screen name="Beranda" component={Home} options={{ headerShown: false }} />
            <Tab.Screen name="Portofolio" component={Portofolio} options={{ headerShown: false }} />
            <Tab.Screen name="Notifikasi" component={Notifikasi} options={{ headerShown: false }} />
            <Tab.Screen name="Profil" component={Profil} options={{ headerShown: false }} />
        </Tab.Navigator>
    );
};

const Router = () => {
    useEffect(() => {
        token();
    }, []);
    return (
        <Stack.Navigator initialRouteName="Splash">
            <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false }} />
            <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="Boarding" component={Boarding} options={{ headerShown: false }} />
            <Stack.Screen name="Analyze" component={Analyze} options={{ headerShown: false }} />
            <Stack.Screen name="EditParameter" component={EditParameter} options={{ headerShown: false }} />
            <Stack.Screen name="OpsiInvestasi" component={OpsiInvestasi} options={{ headerShown: false }} />
            <Stack.Screen name="DetailOpsiInvestasi" component={DetailOpsiInvestasi} options={{ headerShown: false }} />
            <Stack.Screen name="Direct" component={Direct} options={{ headerShown: false }} />
            <Stack.Screen name="DetailDirectInvest" component={DetailDirectInvest} options={{ headerShown: false }} />
            <Stack.Screen name="Questions" component={Questions} options={{ headerShown: false }} />
            <Stack.Screen name="ProfilRisiko" component={ProfilRisiko} options={{ headerShown: false }} />
            <Stack.Screen name="SuccessPengajuan" component={SuccessPengajuan} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
};

export default Router;

