import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Box = ({ color, text }) => {
    return (
        <View style={styles.boxContent}>
            <View style={[styles.box, { backgroundColor: color }]} />
            <Text style={styles.boxText}>{text}</Text>
        </View>
    );
};

export default Box;

const styles = StyleSheet.create({
    boxContent: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: 25,
    },
    box: {
        width: 17,
        height: 17,
        borderRadius: 6,
    },
    boxText: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Regular',
        paddingLeft: 16,
    },
});
