import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Button from '../Button';
import { IconPengajuan } from '../../assets';

const SuccessPengajuan = ({ navigation }) => {
    return (
        <ScrollView>
            <View style={styles.container}>
                <IconPengajuan />
                <Text style={styles.title}>Pengajuan Berhasil</Text>
                <Text style={styles.desc}>Selamat, pengajuan pembelian produk investasi Anda berhasil, selanjutnya akan kami proses, maksimal 2x24 jam.</Text>
                <View style={styles.btn}>
                    <Button textBtn="Selesai" onPress={() => {
                        navigation.replace('MainApp');
                    }} />
                </View>
            </View>
        </ScrollView>
    );
};

export default SuccessPengajuan;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        paddingTop: 100,
        display: 'flex',
        height: Dimensions.get('window').height,
    },
    title: {
        paddingTop: 50,
        fontSize: 20,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
        paddingBottom: 16,
    },
    desc: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
        paddingHorizontal: 40,
        textAlign: 'center',
    },
    btn: {
        marginBottom: 20,
        marginTop: 70,
    },
    image: {
        width: 290,
        height: 272,
        resizeMode: 'cover',
    },
});
