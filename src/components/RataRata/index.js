import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import formatNumber from '../../utils/formatNumber';

const RataRata = ({ title, price }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.price}>Rp {formatNumber(price)}</Text>
        </View>
    );
};

export default RataRata;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 66,
        borderRadius: 9,
        backgroundColor: '#F4F4F5',
    },
    title: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Regular',
        paddingLeft: 16,
        paddingTop: 8,
    },
    price: {
        fontSize: 18,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
        paddingTop: 6,
        paddingLeft: 16,
    },
});
