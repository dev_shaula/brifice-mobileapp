import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { profilRisiko as risiko } from '../../assets';
import { getOpsiAnalyzeRecommend } from '../../redux/actions';
import Button from '../Button';
import Header from '../Header';

const ProfilRisiko = ({ navigation }) => {
    const { score } = useSelector(state => state.analyze);
    const { token } = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const profilRisiko = score < 12 ? 'KONSERVATIF' : score < 23 ? 'MODERAT' : score >= 23 && 'AGRESIF';

    return (
        <ScrollView>
            <View>
                <Header haveArrow actionIcon={() => navigation.goBack(null)} />
                <View style={styles.container}>
                    <Image source={risiko} style={styles.image} />
                    <Text style={styles.title}>PROFIL RISIKO ANDA:</Text>
                    <View style={styles.result}>
                        <Text style={styles.resultText}>{profilRisiko}</Text>
                    </View>
                    <Text style={styles.desc}>Berdasarkan hasil dari jawaban anda, anda mempunyai profil risiko investasi yang tergolong <Text style={{ fontFamily: 'OpenSans-Bold' }}>{profilRisiko}</Text>, dan kami sudah menyiapkan produk investasi yang cocok untuk anda.</Text>
                    <View style={styles.btn}>
                        <Button textBtn="Lihat Rekomendasi Produk Investasi" onPress={() => {
                            dispatch(getOpsiAnalyzeRecommend(token));
                            navigation.replace('OpsiInvestasi');
                        }} />
                    </View>
                </View>
            </View>
        </ScrollView>
    );
};

export default ProfilRisiko;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
        paddingTop: 112,
    },
    image: {
        width: 290,
        height: 272,
        resizeMode: 'cover',
    },
    title: {
        paddingTop: 12,
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    result: {
        width: 260,
        height: 48,
        backgroundColor: 'rgba(106, 166, 255, 0.06)',
        borderWidth: 1,
        borderColor: '#6AA6FF',
        borderRadius: 8,
        marginTop: 16,
        marginBottom: 31,
    },
    resultText: {
        fontSize: 20,
        color: '#6AA6FF',
        fontFamily: 'OpenSans-Bold',
        textAlign: 'center',
        paddingVertical: 10,
    },
    desc: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
        paddingHorizontal: 40,
        textAlign: 'center',
    },
    btn: {
        marginBottom: 20,
        marginTop: 70,
    },
});
