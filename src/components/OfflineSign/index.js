import React from 'react';
import { Dimensions, StyleSheet, Text, View } from 'react-native';
const { width } = Dimensions.get('window');

const Offline = () => {
    return (
        <View style={styles.offlineContainer}>
            <Text style={styles.offlineText}>No Internet Connection</Text>
        </View>
    );
};

export default Offline;

const styles = StyleSheet.create({
    offlineContainer: {
        backgroundColor: '#b52424',
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width,
        position: 'absolute',
        zIndex: 9999,
        top: 0,
    },
    offlineText: {
        color: '#fff',
    },
});
