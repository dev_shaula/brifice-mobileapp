import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Input = ({ placeholder, label, iconType, password, onChange, name, error, ...res }) => {
    return (
        <View style={error ? styles.inputError : styles.inputContainer}>
            <View>
                <Ionicons style={styles.iconStyle} name={iconType} size={25} color="#D0D0D0" />
            </View>
            <TextInput
                name={name}
                style={styles.input}
                placeholder={placeholder}
                placeholderTextColor="#666"
                numberOfLines={1}
                value={label}
                secureTextEntry={password}
                onChangeText={onChange}
                {...res}
            />
        </View>
    );
};

export default Input;

const styles = StyleSheet.create({
    input: {
        height: 56,
        borderRadius: 10,
        backgroundColor: '#F9F9F9',
        paddingLeft: 20,
    },
    inputContainer: {
        marginTop: 5,
        marginBottom: 10,
        width: '100%',
        minHeight: 56,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#F9F9F9',
    },
    iconStyle: {
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRightColor: '#D0D0D0',
        borderRightWidth: 1,
    },
    inputError: {
        marginTop: 5,
        marginBottom: 10,
        width: '100%',
        minHeight: 56,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#F9F9F9',
        borderColor: 'red',
        borderWidth: 1,
    },
});
