import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator } from 'react-native';

const Button = ({ textBtn, loading, onPress, disabled, width, height, ...res }) => {
    return (
        <View style={styles.button(disabled, width, height)}>
            <TouchableOpacity
                {...res}
                onPress={onPress}
                disabled={disabled}
            >
                {
                    loading ? (
                        <ActivityIndicator size="large" color="#ffffff" style={{ paddingTop: 10 }} />
                    ) : (
                        <Text style={styles.btnText(disabled, height)}>{textBtn}</Text>
                    )
                }
            </TouchableOpacity>
        </View>
    );
};

export default Button;

const styles = StyleSheet.create({
    button: (disabled, width, height) => (
        {
            width: width ? width : 327,
            height: height ? height : 54,
            backgroundColor: disabled ? '#F8F8F8' : '#2736C1',
            color: disabled ? '#C4C4C4' : 'white',
            borderRadius: 10,
            display: 'flex',
            alignItems: 'center',
        }
    ),
    btnText: (disabled, height) => (
        {
            color: disabled ? '#C4C4C4' : '#FFFFFF',
            fontSize: 14,
            textAlign: 'center',
            fontFamily: 'OpenSans-Bold',
            paddingVertical: height ? 10 : 18,
        }
    ),
});
