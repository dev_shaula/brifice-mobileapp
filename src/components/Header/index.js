import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Header = ({ headerTitle, actionIcon, actionText, haveArrow }) => {
    return (
        <View>
            {
                haveArrow ? (
                    <View style={styles.headerWithArrow}>
                        <TouchableOpacity onPress={actionIcon}>
                            <Ionicons name="chevron-back-outline" size={30} color="#000000" />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={actionText}>
                            <Text style={styles.title}>{headerTitle}</Text>
                        </TouchableOpacity>
                    </View>
                ) : (
                    <View style={styles.header}>
                        <TouchableOpacity onPress={actionText}>
                            <Text style={styles.headerTitle}>{headerTitle}</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
        </View>
    );
};

export default Header;

const styles = StyleSheet.create({
    header: {
        width: '100%',
        minHeight: '8%',
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 10,
        borderBottomWidth: 0.3,
        borderBottomColor: '#939393',
    },
    title: {
        fontSize: 18,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
        paddingTop: 5,
        paddingLeft: 39,
    },
    headerTitle: {
        fontSize: 18,
        color: '#4A4A4A',
        padding: 10,
        fontFamily: 'OpenSans-Bold',
    },
    headerWithArrow: {
        width: '100%',
        minHeight: '8%',
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        display: 'flex',
        padding: 10,
        borderBottomWidth: 0.3,
        borderBottomColor: '#939393',
    },
});
