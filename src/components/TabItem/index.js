import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { IconBeranda, IconBerandaActive, IconNotifikasi, IconNotifikasiActive, IconPortofolio, IconPortofolioActive, IconProfil, IconProfilActive } from '../../assets';

const TabItem = ({ title, active, onPress, onLongPress }) => {
    const Icon = () => {
        if (title === 'Beranda') {
            return active ? <IconBerandaActive /> : <IconBeranda />;
        } else if (title === 'Portofolio') {
            return active ? <IconPortofolioActive /> : <IconPortofolio />;
        } else if (title === 'Notifikasi') {
            return active ? <IconNotifikasiActive /> : <IconNotifikasi />;
        } else {
            return active ? <IconProfilActive /> : <IconProfil />;
        }
    };
    return (
        <TouchableOpacity style={styles.container} onPress={onPress} onLongPress={onLongPress}>
            <Icon />
            <Text style={styles.text(active)}>{title}</Text>
        </TouchableOpacity>
    );
};

export default TabItem;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
    },
    text: (active) => (
        {
            fontSize: 10,
            fontWeight: active ? '400' : '300',
            color: active ? '#3D5BD5' : '#D0D0D0',
            fontFamily: active ? 'OpenSans-Regular' : 'OpenSans-Light',
        }
    ),
});
