import React from 'react';
import { View, Modal, StyleSheet, Image, Dimensions } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ImageZoom from 'react-native-image-pan-zoom';

const PopUp = (props) => {
    const { visible, onClose, image } = props;

    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={visible}
                onRequestClose={onClose}
            >
                <View style={styles.centeredView}>
                    <MaterialIcons
                        name="close"
                        color="#FFFFFF"
                        size={50}
                        style={styles.icon}
                        onPress={onClose}
                    />
                    <View style={styles.modalView}>
                        <ImageZoom cropWidth={400} cropHeight={Dimensions.get('window').height - 100} imageHeight={300} imageWidth={500}>
                            <Image source={image} style={styles.bannerImage} />
                        </ImageZoom>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

export default PopUp;

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        position: 'relative',
    },
    modalTitle: {
        marginBottom: 15,
        textAlign: 'center',
        fontSize: 14,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    modalDesc: {
        marginBottom: 15,
        textAlign: 'center',
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
    },
    button: {
        borderRadius: 36,
        padding: 10,
        elevation: 2,
        width: 175,
    },
    buttonOk: {
        backgroundColor: '#2736C1',
    },
    buttonClose: {
        backgroundColor: 'white',
        elevation: 0,
    },
    okText: {
        color: 'white',
        textAlign: 'center',
        fontSize: 14,
        fontFamily: 'OpenSans-Bold',
    },
    cancelText: {
        color: '#5683EC',
        textAlign: 'center',
        fontSize: 14,
        fontFamily: 'OpenSans-Bold',
    },
    bannerImage: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
        borderRadius: 8,
    },
    icon: {
        position: 'absolute',
        top: 0,
        right: 0,
    },
    modalView: {
        position: 'relative',
    },
});
