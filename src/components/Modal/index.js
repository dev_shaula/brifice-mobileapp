import React from 'react';
import { StyleSheet, Modal as ModalContainer, View, Text, Pressable } from 'react-native';

const Modal = (props) => {
    const { visible, onClose, title, desc, okText, cancelText, onOk } = props;
    return (
        <View style={styles.centeredView}>
            <ModalContainer
                animationType="slide"
                transparent={true}
                visible={visible}
                onRequestClose={onClose}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalTitle}>{title}</Text>
                        <Text style={styles.modalDesc}>{desc}</Text>
                        <Pressable
                            style={[styles.button, styles.buttonOk]}
                            onPress={onOk}
                        >
                            <Text style={styles.okText}>{okText}</Text>
                        </Pressable>
                        {
                            cancelText && (
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={onClose}
                                >
                                    <Text style={styles.cancelText}>{cancelText}</Text>
                                </Pressable>
                            )
                        }
                    </View>
                </View>
            </ModalContainer>
        </View>
    );
};

export default Modal;

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: 260,
    },
    modalTitle: {
        marginBottom: 15,
        textAlign: 'center',
        fontSize: 14,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    modalDesc: {
        marginBottom: 15,
        textAlign: 'center',
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
    },
    button: {
        borderRadius: 36,
        padding: 10,
        elevation: 2,
        width: 175,
    },
    buttonOk: {
        backgroundColor: '#2736C1',
    },
    buttonClose: {
        backgroundColor: 'white',
        elevation: 0,
    },
    okText: {
        color: 'white',
        textAlign: 'center',
        fontSize: 14,
        fontFamily: 'OpenSans-Bold',
    },
    cancelText: {
        color: '#5683EC',
        textAlign: 'center',
        fontSize: 14,
        fontFamily: 'OpenSans-Bold',
    },
});
