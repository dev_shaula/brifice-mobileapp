import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Header from '../Header';
import Question from '../../json/Question.json';
import Button from '../Button';
import { useDispatch, useSelector } from 'react-redux';
import { getUser, questions, riskProfil } from '../../redux/actions';

const Questions = ({ navigation }) => {
    const [number, setNumber] = useState(0);
    const [score, setScore] = useState(0);
    const [active, setActive] = useState(false);
    const [index, setIndex] = useState(0);
    const { token } = useSelector(state => state.auth);
    const { loading } = useSelector(state => state.riskProfil);
    const dispatch = useDispatch();
    const dataQuestion = Question[number];
    const profilRisiko = score < 12 ? 'KONSERVATIF' : score < 23 ? 'MODERAT' : score >= 23 && 'AGRESIF';

    return (
        <View>
            <Header haveArrow actionIcon={() => navigation.goBack(null)} headerTitle="Short Question" />
            <View style={styles.container}>
                <Text style={styles.title}>Pertanyaan {number + 1} dari {Question.length}</Text>
                <Text style={styles.questions}>{dataQuestion.question}</Text>
                {
                    dataQuestion.answers.map((val, idx) => (
                        <TouchableOpacity key={idx} onPress={() => {
                            setActive(true);
                            setIndex(idx);
                            setScore(prev => prev + val.poin);
                        }}>
                            <View style={[styles.listAnswer, active && index === idx ? styles.answerActive : { borderColor: '#C8C8C8' }]}>
                                <View style={styles.answer}>
                                    <Text style={[styles.text, { color: active && index === idx ? '#6AA6FF' : '#4A4A4A' }]}>{val.abjad}</Text>
                                    <Text style={[styles.text, { color: active && index === idx ? '#6AA6FF' : '#4A4A4A' }]}>{val.answer}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    ))
                }
                <View style={styles.btn}>
                    <Button loading={loading} disabled={!active} textBtn={number + 1 === Question.length ? 'Selesai' : 'Selanjutnya'} onPress={() => {
                        if (number + 1 === Question.length) {
                            setActive(false);
                            dispatch(questions({
                                score,
                            }));
                            dispatch(getUser(token));
                            dispatch(riskProfil(token, score, profilRisiko));
                            navigation.replace('ProfilRisiko');
                        } else {
                            setNumber(prev => prev + 1);
                            setActive(false);
                        }
                    }} />
                </View>
            </View>
        </View>
    );
};

export default Questions;

const styles = StyleSheet.create({
    container: {
        minHeight: 800,
        backgroundColor: 'white',
        paddingTop: 40,
        paddingLeft: 32,
        paddingRight: 32,
    },
    title: {
        color: '#4A4A4A',
        fontSize: 12,
        fontFamily: 'OpenSans-Regular',
        paddingBottom: 10,
    },
    questions: {
        color: '#4A4A4A',
        fontSize: 14,
        fontFamily: 'OpenSans-SemiBold',
        paddingBottom: 32,
    },
    listAnswer: {
        width: '100%',
        minHeight: 48,
        borderWidth: 0.5,
        borderColor: '#C8C8C8',
        borderRadius: 8,
        marginBottom: 16,
    },
    answer: {
        display: 'flex',
        flexDirection: 'row',
        paddingVertical: 15,
        paddingRight: 16,
    },
    text: {
        paddingLeft: 8,
        fontSize: 12,
        fontFamily: 'OpenSans-Regular',
    },
    answerActive: {
        width: '100%',
        minHeight: 48,
        borderWidth: 1,
        borderColor: '#6AA6FF',
        borderRadius: 8,
        marginBottom: 16,
    },
    btn: {
        marginBottom: 16,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
