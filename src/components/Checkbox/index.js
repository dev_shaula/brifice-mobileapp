import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const Checkbox = ({ selected, text, onPress, id }) => {
    return (
        <TouchableOpacity style={styles.checkBox} onPress={onPress}>
            <MaterialIcons
                name={selected ? 'check-box' : 'check-box-outline-blank'}
                color="#2736C1"
                size={30}
            />
            <Text style={{ paddingLeft: 10, paddingRight: 50 }}>{text}</Text>
        </TouchableOpacity>
    );
};

export default Checkbox;

const styles = StyleSheet.create({
    checkBox: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 20,
    },
});
