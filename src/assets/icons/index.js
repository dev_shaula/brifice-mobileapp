import IconBeranda from './ic_beranda.svg';
import IconBerandaActive from './ic_beranda_active.svg';
import IconPortofolio from './ic_portofolio.svg';
import IconPortofolioActive from './ic_portofolio_active.svg';
import IconNotifikasiActive from './ic_notifikasi_active.svg';
import IconNotifikasi from './ic_notifikasi.svg';
import IconProfil from './ic_profil.svg';
import IconProfilActive from './ic_profil_active.svg';
import IconWarning from './ic_warning.svg';
import IconFaq from './ic_faq.svg';
import IconTerms from './ic_terms.svg';
import IconSignOut from './ic_signout.svg';
import IconArrow from './ic_arrow.svg';
import IconBrifine from './ic_brifine.svg';
import IconDavestera from './ic_davestera.svg';
import IconCalculator from './ic_calculator.svg';
import IconInvest from './ic_invest.svg';
import IconPengajuan from './ic_pengajuan.svg';
import IconBritama from './ic_britama.svg';

export {
    IconBeranda,
    IconBerandaActive,
    IconProfilActive,
    IconProfil,
    IconPortofolio,
    IconPortofolioActive,
    IconNotifikasi,
    IconNotifikasiActive,
    IconWarning,
    IconFaq,
    IconTerms,
    IconSignOut,
    IconArrow,
    IconBrifine,
    IconDavestera,
    IconInvest,
    IconCalculator,
    IconPengajuan,
    IconBritama,
};
