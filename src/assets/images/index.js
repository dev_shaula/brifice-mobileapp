import Logo from './logo.png';
import onBoarding1 from './monitoring.png';
import onBoarding2 from './hitung.png';
import onBoarding3 from './opsi.png';
import dummyProfil from './dummyProfile.png';
import background from './background.png';
import analyzeEmpty from './analyze-empty.png';
import investEmpty from './invest-empty.png';
import profilRisiko from './profil-risiko.png';
import bannerExample from './banner-example.png';
import deposito from './deposito.png';
import dplk from './dplk.png';
import reksadana from './reksadana.png';
import britama from './britama.png';
import banner1 from './banner1.jpeg';
import banner2 from './banner2.jpeg';

export { Logo, onBoarding1, onBoarding2, onBoarding3, dummyProfil, background, analyzeEmpty, investEmpty, profilRisiko, bannerExample, deposito, dplk, reksadana, britama, banner1, banner2 };
