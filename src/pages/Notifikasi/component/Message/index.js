import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Message = ({ date, title, desc }) => {
    return (
        <View style={styles.message}>
            <View style={styles.content}>
                <Text style={styles.date}>{date}</Text>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.desc}>{desc}</Text>
            </View>
        </View>
    );
};

export default Message;

const styles = StyleSheet.create({
    message: {
        backgroundColor: 'white',
        minHeight: 120,
        borderBottomWidth: 0.3,
        borderBottomColor: '#939393',
    },
    content: {
        paddingHorizontal: 24,
    },
    date: {
        fontSize: 10,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
        paddingTop: 20,
    },
    title: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-SemiBold',
        paddingTop: 8,
    },
    desc: {
        fontSize: 10,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
        paddingTop: 4,
        paddingBottom: 20,
    },
});
