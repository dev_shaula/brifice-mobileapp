import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Header } from '../../components';
import { Message } from './component';

const Notifikasi = () => {
    return (
        <ScrollView>
            <View>
                <Header headerTitle="Notifikasi" />
                <View style={styles.container}>
                    <Message
                        title="Ayo perbarui aplikasi BRIFice mu!"
                        desc="Segera perbarui aplikasi Brifice anda ke versi 10.10.10. Demi kenyamanan anda."
                        date="2 jam yang lalu"
                    />
                    <Message
                        title="Pengajuan Pembelian Produk Investasi Sukses!"
                        desc="Pengajuan anda sudah kami terima dan akan kami proses paling lama 2x24 jam hari kerja. Terima Kasih."
                        date="Kemarin, 20:00"
                    />
                    <Message
                        title="Produk investasi baru untuk anda!"
                        desc="mari berinvestasi diproduk baru kami, BRITAMA  dan dapatkan banyak keuntungan!"
                        date="Kemarin, 07:12"
                    />
                    <Message
                        title="Pemeliharaan Sistem"
                        desc="Pemberitahuan : Akan dijadwalkan Pemeliharaan sistem rutin  pada 2 Juli 2021 pukul 22:00 - 23:59. Terima Kasih"
                        date="1 Juli 2021, 12:00"
                    />
                    <Message
                        title="Ayo perbarui aplikasi BRIFice mu!"
                        desc="Segera perbarui aplikasi Brifice anda ke versi 9.10.10. Demi kenyamanan anda."
                        date="1 Juli 2021, 11:00"
                    />
                    <Message
                        title="Selamat Pembelian Berhasil."
                        desc="Pembelian Produk investasi DAVESTARA sebesar Rp 2.000.000 Telah berhasil. Cek protofolio anda sekarang!"
                        date="30 Juni 2021, 13:00"
                    />
                    <Message
                        title="Selamat Pembelian Berhasil."
                        desc="Pembelian Produk investasi DAVESTARA sebesar Rp 2.000.000 Telah berhasil. Cek protofolio anda sekarang!"
                        date="30 Juni 2021, 13:00"
                    />
                </View>
            </View>
        </ScrollView>
    );
};

export default Notifikasi;

const styles = StyleSheet.create({
    container: {
        minHeight: 800,
    },
});
