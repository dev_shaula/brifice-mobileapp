/* eslint-disable react-hooks/exhaustive-deps */
import React, { createRef, useCallback, useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, RefreshControl } from 'react-native';
import { Header, Offline } from '../../components';
import { dummyProfil, IconWarning, IconTerms, IconFaq, IconSignOut, IconArrow } from '../../assets';
import { useDispatch, useSelector } from 'react-redux';
import { getUser, logout } from '../../redux/actions';
import ImageCropPicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actions-sheet';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NetInfo from '@react-native-community/netinfo';

const Profil = ({ navigation }) => {
    const { result } = useSelector(state => state.userProfil);
    const { token } = useSelector(state => state.auth);
    const [image, setImage] = useState('');
    const [openRekening, setOpenRekening] = useState(false);
    const [refreshing, setRefreshing] = useState(false);
    const dispatch = useDispatch();
    const actionSheetRef = createRef();
    const [connect, setConnect] = useState(true);

    const chooseGallery = () => {
        actionSheetRef.current?.hide();
        ImageCropPicker.openPicker({
            width: 90,
            height: 90,
            compressImageQuality: 0.7,
            cropping: true,
        }).then(response => {
            setImage(response.path);
        });
    };

    const takePhoto = () => {
        actionSheetRef.current?.hide();
        ImageCropPicker.openCamera({
            width: 90,
            height: 90,
            compressImageQuality: 0.7,
            cropping: true,
        }).then(response => {
            setImage(response.path);
        });
    };

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    };

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        dispatch(getUser(token));
        wait(2000).then(() => {
            setRefreshing(false);
        });
    }, []);

    useEffect(() => {
        const get = async () => {
            try {
                const tokenLogin = await AsyncStorage.getItem('token');
                if (!tokenLogin) {
                    dispatch(logout());
                    navigation.replace('Login');
                    alert('Sesi telah berakhir, silahkan login kembali');
                } else {
                    dispatch(getUser(token ? token : tokenLogin));
                }
            } catch (err) {
                console.log('profil', err);
            }
        };
        get();
    }, [token, result.profil_resiko]);

    useEffect(() => {
        const unsubscribe = NetInfo.addEventListener(state => {
            setConnect(state.isConnected);
        });
        unsubscribe();
    });

    return (
        <ScrollView
            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
        >
            <View>
                {
                    !connect && (
                        <Offline />
                    )
                }
                <Header headerTitle="Profil" />
                <View style={styles.container}>
                    <View style={styles.profilContainer}>
                        <Image style={styles.image} source={image ? { uri: image } : dummyProfil} />
                        <TouchableOpacity onPress={() => actionSheetRef.current?.setModalVisible()}>
                            <Text style={{ color: '#5683EC', fontWeight: '600', fontSize: 11, fontFamily: 'OpenSans-SemiBold' }}>Ubah Foto</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.sectionData}>
                        <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row' }}>
                            <Text style={styles.label}>Nama</Text>
                            <Text style={styles.value}>{result ? result.nama : '-'}</Text>
                        </View>
                        <View style={{ display: 'flex', justifyContent: 'space-between', marginTop: 10, flexDirection: 'row' }}>
                            <Text style={styles.label}>Nomor Rekening BRI</Text>
                            {
                                result === 'Rekening Not Found!' ? (
                                    <Text>-</Text>
                                ) : (
                                    <MaterialIcons size={30} name={`${openRekening ? 'keyboard-arrow-up' : 'keyboard-arrow-down'}`} onPress={() => setOpenRekening(!openRekening)} />
                                )
                            }
                        </View>
                        {
                            openRekening && (
                                Array.isArray(result.nomor_rekening) ? result.nomor_rekening.map((val, idx) => (
                                    <View style={{ display: 'flex', justifyContent: 'flex-end', marginTop: 10, flexDirection: 'column', alignItems: 'flex-end' }} key={idx}>
                                        <Text style={styles.value}>{val}</Text>
                                    </View>
                                ))
                                    : (
                                        <View style={{ display: 'flex', justifyContent: 'flex-end', flexDirection: 'column', alignItems: 'flex-end' }}>
                                            <Text style={styles.value}>{result.nomor_rekening}</Text>
                                        </View>
                                    )
                            )
                        }
                        <View style={{ display: 'flex', justifyContent: 'space-between', marginTop: openRekening ? 20 : 10, flexDirection: 'row' }}>
                            <Text style={styles.label}>Profile Risiko</Text>
                            <View style={styles.status}>
                                <Text style={{ color: '#5683EC', fontFamily: 'OpenSans-SemiBold', fontSize: 12 }}>{result.profil_resiko ? result.profil_resiko : '-'}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.settings}>
                        <TouchableOpacity onPress={() => navigation.navigate('Questions', { router: 'Profil' })}>
                            <View style={styles.settingsData}>
                                <View style={{ display: 'flex', flexDirection: 'row' }}>
                                    <IconWarning />
                                    <Text style={{ paddingLeft: 10, fontFamily: 'OpenSans-Regular' }}>Ubah Profil Risiko</Text>
                                </View>
                                <IconArrow style={{ marginTop: 5 }} />
                            </View>
                        </TouchableOpacity>
                        <View style={styles.settingsData}>
                            <View style={{ display: 'flex', flexDirection: 'row' }}>
                                <IconFaq />
                                <Text style={{ paddingLeft: 10, fontFamily: 'OpenSans-Regular' }}>FAQ</Text>
                            </View>
                            <IconArrow style={{ marginTop: 5 }} />
                        </View>
                        <View style={styles.settingsData}>
                            <View style={{ display: 'flex', flexDirection: 'row' }}>
                                <IconTerms />
                                <Text style={{ paddingLeft: 10, fontFamily: 'OpenSans-Regular' }}>Term & Conditions</Text>
                            </View>
                            <IconArrow style={{ marginTop: 5 }} />
                        </View>
                        <TouchableOpacity onPress={() => {
                            dispatch(logout());
                            navigation.replace('Login');
                        }}>
                            <View style={styles.settingsData}>
                                <View style={{ display: 'flex', flexDirection: 'row' }}>
                                    <IconSignOut style={{ marginTop: 3 }} />
                                    <Text style={{ paddingLeft: 10, fontFamily: 'OpenSans-Regular' }}>Sign Out</Text>
                                </View>
                                <IconArrow style={{ marginTop: 5 }} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ textAlign: 'center', paddingTop: 30, paddingBottom: 30, fontFamily: 'OpenSans-Regular' }}>Version 0.0.0.1</Text>
                </View>
            </View>
            <ActionSheet ref={actionSheetRef}>
                <Text style={styles.update}>Update foto profil</Text>
                <TouchableOpacity onPress={takePhoto}>
                    <View style={styles.modalBottom}>
                        <Text style={{ color: '#5683EC' }}>Buka kamera</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={chooseGallery}>
                    <View style={styles.modalBottom}>
                        <Text style={{ color: '#5683EC' }}>Pilih dari gallery</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    actionSheetRef.current?.hide();
                }}>
                    <View style={styles.modalBottom}>
                        <Text style={{ color: '#5683EC' }}>Cancel</Text>
                    </View>
                </TouchableOpacity>
            </ActionSheet>
        </ScrollView>
    );
};

export default Profil;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    profilContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: 200,
    },
    sectionData: {
        height: 200,
        marginTop: 20,
        backgroundColor: 'white',
        padding: 20,
        display: 'flex',
        justifyContent: 'space-evenly',
    },
    label: {
        fontFamily: 'OpenSans-Regular',
        color: '#4A4A4A',
        fontSize: 14,
    },
    value: {
        fontFamily: 'OpenSans-SemiBold',
        color: '#4A4A4A',
        fontSize: 16,
    },
    status: {
        width: 'auto',
        height: 'auto',
        borderWidth: 1,
        borderColor: '#6AA6FF',
        borderRadius: 26,
        padding: 8,
        backgroundColor: 'rgba(106, 166, 255, 0.2)',
    },
    settings: {
        minHeight: 200,
        marginTop: 20,
        backgroundColor: 'white',
        padding: 20,
        display: 'flex',
        justifyContent: 'space-around',
    },
    settingsData: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: '#D0D0D0',
        borderBottomWidth: 1,
        paddingTop: 10,
        paddingBottom: 20,
        marginTop: 10,
        marginBottom: 10,
    },
    image: {
        width: 90,
        height: 90,
        borderRadius: 50,
        borderWidth: 0.5,
        borderColor: 'blue',
    },
    modalBottom: {
        width: '100%',
        height: 50,
        borderColor: '#DDDDDD',
        borderWidth: 0.5,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    update: {
        textAlign: 'center',
        paddingVertical: 20,
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Regular',
    },
});
