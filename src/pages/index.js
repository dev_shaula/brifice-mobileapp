import Home from './Home';
import Login from './Login';
import Notifikasi from './Notifikasi';
import Portofolio from './Portofolio';
import Profil from './Profil';
import Splash from './Splash/Splash';
import Boarding from './Boarding';
import Analyze from './Analyze';
import Direct from './Direct';
import EditParameter from './Analyze/editParameter';
import OpsiInvestasi from './Analyze/opsiInvestasi';
import DetailOpsiInvestasi from './Analyze/detailOpsiInvestasi';
import DetailDirectInvest from './Direct/detailDirectInvest';

export { Home, Login, Notifikasi, Portofolio, Profil, Splash, Boarding, Analyze, Direct, EditParameter, OpsiInvestasi, DetailOpsiInvestasi, DetailDirectInvest };
