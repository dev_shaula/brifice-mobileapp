import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Input } from '../../components';
import { signIn } from '../../redux/actions';

const Login = ({ navigation }) => {
	const [login, setLogin] = useState({
		username: '',
		password: '',
	});
	const dispatch = useDispatch();
	const authState = useSelector(state => state.auth);
	const [error, setError] = useState(false);
	const [errorMessage, setErrorMessage] = useState('');

	const auth = () => {
		dispatch(signIn(login, (status, message) => {
			if (status === 'error' || !status) {
				setError(true);
				setErrorMessage(message === 'Network Error' ? 'Tidak ada internet.' : message === 'Password Incorrect!' ? 'Password salah.' : message === 'username/email incorrect' ? 'Akun tidak ditemukan, periksa kembali username anda.' : message);
			} else {
				setError(false);
				navigation.replace('Boarding');
			}
		}));
	};
	return (
		<View style={styles.container}>
			<Text style={styles.welcomeText}>Selamat Datang</Text>
			<Text style={styles.welcomeText}>di <Text style={styles.briText}>BRI<Text style={{ color: '#FF9D07' }}>FICE</Text></Text></Text>
			<Text style={styles.descText}>Silakan masukkan username dan password yang sama dengan yang anda gunakan pada aplikasi BRIMO untuk login.</Text>
			<View style={styles.form}>
				<Input
					iconType="person-outline"
					name="username"
					placeholder="Username"
					label={login.username}
					onChange={(text) => {
						setLogin({ ...login, username: text });
						setError(false);
						setErrorMessage('');
					}}
					error={errorMessage === 'Akun tidak ditemukan, periksa kembali username anda.'}
				/>
				<Input
					iconType="lock-closed-outline"
					placeholder="Password"
					password={true}
					label={login.password}
					name="password"
					onChange={(text) => {
						setLogin({ ...login, password: text });
						setError(false);
						setErrorMessage('');
					}}
					error={error}
				/>
				<Text style={styles.errorText}>{errorMessage}</Text>
			</View>
			<View style={styles.btnLogin}>
				<Button textBtn="Masuk" onPress={auth} loading={authState.loading} />
			</View>
		</View>
	);
};

export default Login;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#FFFFFF',
		position: 'relative',
	},
	welcomeText: {
		fontSize: 32,
		paddingLeft: 20,
		color: '#4A4A4A',
		fontFamily: 'OpenSans-Bold',
	},
	briText: {
		color: '#2736C1',
	},
	descText: {
		paddingLeft: 20,
		color: '#4A4A4A',
		fontSize: 12,
		paddingRight: 80,
		paddingTop: 15,
		fontFamily: 'OpenSans-Light',
	},
	btnLogin: {
		position: 'absolute',
		bottom: 0,
		marginBottom: 10,
		alignSelf: 'center',
	},
	form: {
		paddingLeft: 20,
		paddingRight: 20,
		marginTop: 30,
		marginBottom: 60,
	},
	errorText: {
		color: 'red',
	},
});
