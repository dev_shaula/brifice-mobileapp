import React from 'react';
import { StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { IconCalculator, IconInvest } from '../../../../assets';

const BerandaButton = ({ navigation, isCalculator, title, desc }) => {
    return (
        <TouchableWithoutFeedback onPress={navigation}>
            <View style={styles.analyze}>
                {
                    isCalculator ? (
                        <IconCalculator style={[styles.icon, { marginLeft: 5 }]} />
                    ) : (
                        <IconInvest style={[styles.icon, { marginLeft: 5 }]} />
                    )
                }
                <View style={styles.text}>
                    <Text style={styles.contentTitle}>{title}</Text>
                    {desc}
                </View>
                <Ionicons style={[styles.icon, { marginTop: 40 }]} name="chevron-forward-outline" size={30} color="#5683EC" />
            </View>
        </TouchableWithoutFeedback>
    );
};

export default BerandaButton;

const styles = StyleSheet.create({
    analyze: {
        display: 'flex',
        flexDirection: 'row',
        minHeight: 108,
        width: '100%',
        borderRadius: 6,
        backgroundColor: '#F1F6FF',
        marginBottom: 20,
        justifyContent: 'space-between',
    },
    text: {
        paddingLeft: 10,
        paddingTop: 30,
    },
    icon: {
        marginTop: 30,
    },
    contentDesc: {
        fontSize: 10,
        fontFamily: 'OpenSans-Regular',
        width: 240,
    },
    contentTitle: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: '#5683EC',
    },
});
