/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, ImageBackground, TouchableOpacity, RefreshControl } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { dummyProfil, background, banner1, banner2 } from '../../assets';
import { getAnalyze, getUser, logout } from '../../redux/actions';
import { BerandaButton } from './component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Modal, Offline, PopUp } from '../../components';
import NetInfo from '@react-native-community/netinfo';

const Home = ({ navigation }) => {
    const now = new Date();
    const hour = now.getHours();
    const dispatch = useDispatch();
    const { result, error } = useSelector(state => state.userProfil);
    const { token } = useSelector(state => state.auth);
    const [open, setOpen] = useState(false);
    const [image, setImage] = useState('');
    const [refreshing, setRefreshing] = useState(false);
    const [connect, setConnect] = useState(true);
    const [openModal, setOpenModal] = useState(false);

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    };

    const onRefresh = useCallback(async () => {
        setRefreshing(true);
        const tokenLogin = await AsyncStorage.getItem('token');
        if (!tokenLogin) {
            setOpenModal(true);
        } else {
            dispatch(getUser(token ? token : tokenLogin));
        }
        wait(2000).then(() => {
            setRefreshing(false);
        });
    }, []);

    useEffect(() => {
        const get = async () => {
            try {
                const tokenLogin = await AsyncStorage.getItem('token');
                const idUser = await AsyncStorage.getItem('id');
                console.log('token login', tokenLogin);
                if (!tokenLogin) {
                    setOpenModal(true);
                } else {
                    dispatch(getUser(token ? token : tokenLogin));
                    tokenLogin && !token && dispatch({
                        type: 'LOGIN_SUCCESS',
                        hasLogin: true,
                        token: tokenLogin,
                        id: Number(idUser),
                    });
                    setOpenModal(false);
                }
            } catch (err) {
                console.log('home', err);
            }
        };
        get();
    }, []);

    useEffect(() => {
        if (error) {
            if (error === 'Token is expired') {
                setOpenModal(true);
            } else {
                setOpenModal(false);
            }
        }
        console.log('error home', error);
    }, [error]);

    useEffect(() => {
        const unsubscribe = NetInfo.addEventListener(state => {
            setConnect(state.isConnected);
        });
        unsubscribe();
    });

    console.log('connect', connect);

    return (
        <ScrollView
            style={styles.container}
            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
        >
            <View>
                {
                    !connect && (
                        <Offline />
                    )
                }
                <View style={styles.profil}>
                    <Image style={styles.image} source={dummyProfil} />
                    <View style={styles.welcome}>
                        <Text style={{ fontWeight: '400', fontSize: 10, color: '#4A4A4A', fontFamily: 'OpenSans-Regular' }}>Selamat {(hour >= 1 && hour <= 11) ? 'Pagi' : (hour >= 12 && hour <= 15) ? 'Siang' : (hour >= 16 && hour <= 18) ? 'Sore' : (hour >= 19 || hour <= 1) && 'Malam'}</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 14, color: '#4A4A4A', fontFamily: 'OpenSans-Bold' }}>{result ? result.nama : '-'}</Text>
                    </View>
                </View>
                <View style={styles.statistik}>
                    <ImageBackground source={background} style={styles.background}>
                        <View style={styles.portofolio}>
                            <Text style={styles.title}>Nilai Portofolio</Text>
                            <Text style={styles.desc}>Rp 325.750.000</Text>
                        </View>
                        <View style={[styles.portofolio, { marginBottom: 25 }]}>
                            <Text style={styles.title}>Total Pendapatan</Text>
                            <Text style={styles.desc}>Rp 50.750.000</Text>
                        </View>
                    </ImageBackground>
                </View>
                <View style={styles.banner}>
                    <Text style={styles.update}>Update</Text>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        <TouchableOpacity onPress={() => {
                            setOpen(true);
                            setImage(banner1);
                        }}>
                            <Image source={banner1} style={styles.bannerImage} resizeMode="cover" />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            setOpen(true);
                            setImage(banner2);
                        }}>
                            <Image source={banner2} style={styles.bannerImage} resizeMode="cover" />
                        </TouchableOpacity>
                    </ScrollView>
                </View>
                <View style={styles.produk}>
                    <BerandaButton
                        navigation={() => {
                            navigation.navigate('Analyze');
                            dispatch(getAnalyze(token));
                        }}
                        title="Analyze"
                        desc={<Text style={styles.contentDesc}>Analisa keuanganmu untuk dapat <Text style={{ color: '#5683EC', fontFamily: 'OpenSans-Bold' }}>kami rekomendasikan</Text> produk investasi yang sesuai</Text>}
                        isCalculator
                    />
                    <BerandaButton
                        navigation={() => navigation.navigate('Direct')}
                        title="Direct Invest"
                        desc={<Text style={styles.contentDesc}>Pilih dan <Text style={{ color: '#5683EC', fontFamily: 'OpenSans-Bold' }}>tentukan sendiri</Text> produk investasi yang ingin kamu beli.</Text>}
                    />
                </View>
            </View>
            {
                open && (
                    <PopUp visible={open} onClose={() => setOpen(false)} image={image} />
                )
            }

            {
                openModal && (
                    <Modal
                        visible={openModal}
                        onClose={() => setOpenModal(false)}
                        title="Sesi Anda telah berakhir, silahkan login kembali"
                        okText="Ya"
                        onOk={() => {
                            setOpenModal(false);
                            dispatch(logout());
                            navigation.replace('Login');
                        }}
                    />
                )
            }
        </ScrollView>
    );
};

export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    image: {
        width: 40,
        height: 40,
        borderRadius: 50,
        resizeMode: 'cover',
    },
    profil: {
        display: 'flex',
        flexDirection: 'row',
        paddingLeft: 18,
        paddingRight: 18,
        paddingTop: 20,
    },
    welcome: {
        paddingLeft: 10,
    },
    statistik: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30,
    },
    portofolio: {
        paddingLeft: 30,
        paddingRight: 18,
        paddingTop: 20,
    },
    title: {
        color: 'white',
        fontSize: 11,
        fontFamily: 'OpenSans-Regular',
    },
    desc: {
        color: 'white',
        fontSize: 18,
        fontFamily: 'OpenSans-Bold',
    },
    background: {
        resizeMode: 'cover',
        width: '100%',
        minHeight: 165,
        borderRadius: 30,
        display: 'flex',
        justifyContent: 'space-between',
    },
    produk: {
        paddingLeft: 18,
        paddingRight: 18,
        marginBottom: 18,
    },
    contentDesc: {
        fontSize: 10,
        fontFamily: 'OpenSans-Regular',
        width: 240,
    },
    contentTitle: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: '#5683EC',
    },
    update: {
        fontSize: 14,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
        paddingVertical: 16,
    },
    bannerImage: {
        width: 290,
        height: 120,
        marginRight: 10,
        borderRadius: 4,
    },
    banner: {
        marginBottom: 42,
        paddingLeft: 20,
    },
});
