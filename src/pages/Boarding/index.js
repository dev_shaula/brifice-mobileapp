import React, { useRef, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, Dimensions } from 'react-native';
import { Button } from '../../components';
import Ionicons from 'react-native-vector-icons/Ionicons';

const { width, height } = Dimensions.get('window');

const Boarding = ({ navigation }) => {
    const [next, setNext] = useState(0);
    const ref = useRef();
    const onBoarding = [
        {
            id: 1,
            image: require('../../assets/images/monitoring.png'),
            title: 'Monitoring',
            subtitle: 'Monitor pemasukan dan pengeluaran kamu dengan mudah dalam satu tampilan.',
        },
        {
            id: 2,
            image: require('../../assets/images/hitung.png'),
            title: 'Hitung Idle Money',
            subtitle: 'Hitung dan maksimalkan Idle Money kamu untuk sesuatu yang lebih menghasilkan.',
        },
        {
            id: 3,
            image: require('../../assets/images/opsi.png'),
            title: 'Opsi Investasi',
            subtitle: 'Terdapat banyak sekali opsi investasi, kamu dapat memilih sesuai dengan kebutuhanmu.',
        },
    ];

    const Slide = ({ item }) => {
        return (
            <View style={styles.bodyContainer}>
                <Image source={item?.image} style={{ width, height: 257, resizeMode: 'contain' }} />
                <View>
                    <Text style={styles.title}>{item?.title}</Text>
                    <Text style={styles.description}>{item?.subtitle}</Text>
                </View>
            </View>
        );
    };

    const updateCurrIndex = (e) => {
        const contentOffsetX = e.nativeEvent.contentOffset.x;
        const currIndex = Math.round(contentOffsetX / width);
        setNext(currIndex);
    };

    const nextSlide = () => {
        if (next !== 2) {
            const nexSlideIdx = next + 1;
            const offset = nexSlideIdx * width;
            ref?.current.scrollToOffset({ offset });
            setNext(nexSlideIdx);
        } else {
            navigation.replace('MainApp');
        }
    };

    const prevSlide = () => {
        if (next !== 0) {
            const prevSlideIdx = next - 1;
            const offset = prevSlideIdx * width;
            ref?.current.scrollToOffset({ offset });
            setNext(prevSlideIdx);
        } else {
            navigation.replace('Splash');
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={prevSlide}>
                    <Ionicons name="chevron-back-outline" size={30} color="#000000" />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('MainApp')}>
                    <Text style={styles.headerTitle}>Lewati</Text>
                </TouchableOpacity>
            </View>
            <FlatList
                ref={ref}
                onMomentumScrollEnd={updateCurrIndex}
                data={onBoarding}
                contentContainerStyle={{ height: height * 0.75 }}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={({ item }) => <Slide item={item} />}
                keyExtractor={item => item.id}
                pagingEnabled
            />
            <View style={{ justifyContent: 'space-between', height: height * 0.25 }}>
                <View style={styles.dotContainer}>
                    {
                        onBoarding.map((_, idx) => (
                            <View key={`dot-${idx}`} style={next === idx ? styles.dotActive : styles.dot} />
                        ))
                    }
                </View>
                <View style={styles.btnBoarding}>
                    <Button onPress={nextSlide} textBtn={`${next === 2 ? 'Selesai' : 'Selanjutnya'}`} />
                </View>
            </View>
        </View>
    );
};

export default Boarding;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    title: {
        fontSize: 20,
        color: '#4A4A4A',
        textAlign: 'center',
        paddingTop: 20,
        fontFamily: 'OpenSans-Bold',
    },
    description: {
        color: '#4A4A4A',
        fontSize: 12,
        fontFamily: 'OpenSans-Light',
        textAlign: 'center',
        marginTop: 10,
        lineHeight: 18,
        width: 204,
    },
    bodyContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    dotActive: {
        borderRadius: 50,
        backgroundColor: '#2736C1',
        marginHorizontal: 10 / 2,
        marginBottom: 30,
        width: 12,
        height: 12,
    },
    dot: {
        borderRadius: 50,
        backgroundColor: '#F4F4F5',
        marginHorizontal: 10 / 2,
        marginBottom: 30,
        width: 12,
        height: 12,
    },
    dotContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 40,
    },
    btnBoarding: {
        marginBottom: 40,
        alignSelf: 'center',
    },
    header: {
        width: '100%',
        minHeight: '8%',
        backgroundColor: 'white',
        flexDirection: 'row',
        display: 'flex',
        padding: 10,
        justifyContent: 'space-between',
    },
    headerTitle: {
        fontSize: 12,
        color: '#4A4A4A',
        padding: 10,
        fontFamily: 'OpenSans-Regular',
    },
});
