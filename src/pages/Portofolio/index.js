import React from 'react';
import { StyleSheet, Text, View, ScrollView, ImageBackground } from 'react-native';
import { Header } from '../../components';
import { background } from '../../assets';
import Produk from './Produk';

const Portofolio = () => {
    return (
        <ScrollView>
            <View>
                <Header headerTitle="Portofolio" />
                <View style={styles.statistik}>
                    <ImageBackground source={background} style={styles.background}>
                        <View style={styles.portofolio}>
                            <Text style={styles.title}>Nilai Portofolio</Text>
                            <Text style={styles.desc}>Rp 325.750.000</Text>
                        </View>
                        <View style={[styles.portofolio, { marginBottom: 25 }]}>
                            <Text style={styles.title}>Total Pendapatan</Text>
                            <Text style={styles.desc}>Rp 50.750.000</Text>
                        </View>
                    </ImageBackground>
                </View>
                <View style={styles.container}>
                    <Text style={{
                        fontSize: 14,
                        color: '#4A4A4A',
                        paddingTop: 20,
                        fontFamily: 'OpenSans-Bold',
                    }}>List Produk Investasi</Text>
                    <Text style={{
                        fontSize: 11,
                        color: '#4A4A4A',
                        paddingBottom: 20,
                        fontFamily: 'OpenSans-Regular',
                    }}>Lorem ipsum dolor sit amet, consectetur adi.</Text>
                    <Produk name="BRIFINE" nilai={50750000} manfaat={10120000} />
                    <Produk name="BRITAMA" nilai={50750000} manfaat={10120000} />
                    <Produk name="DAVESTERA" nilai={50750000} manfaat={10120000} />
                </View>
            </View>
        </ScrollView>
    );
};

export default Portofolio;

const styles = StyleSheet.create({
    container: {
        paddingLeft: 18,
        paddingRight: 18,
    },
    statistik: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30,
    },
    portofolio: {
        paddingLeft: 30,
        paddingRight: 18,
        paddingTop: 20,
    },
    title: {
        color: 'white',
        fontSize: 11,
        fontFamily: 'OpenSans-Regular',
    },
    desc: {
        color: 'white',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
    },
    background: {
        resizeMode: 'cover',
        width: '100%',
        minHeight: 165,
        borderRadius: 30,
        display: 'flex',
        justifyContent: 'space-between',
    },
});
