import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { IconBrifine, IconBritama, IconDavestera } from '../../assets';
import formatNumber from '../../utils/formatNumber';

const Produk = ({ name, nilai, manfaat }) => {
    return (
        <View style={styles.produk}>
            <View style={styles.namaProduk}>
                {
                    name === 'BRIFINE' ? (
                        <IconBrifine />
                    ) : (
                        name === 'BRITAMA' ? (
                            <IconBritama />
                        ) : (
                            <IconDavestera />
                        )
                    )
                }
                <View style={styles.detailProduk}>
                    <Text style={styles.title}>{name}</Text>
                    <Text style={styles.lihatDetail}>Lihat Detail</Text>
                </View>
            </View>
            <View style={styles.hr} />
            <View style={styles.content}>
                <View>
                    <Text style={{
                        color: '#4A4A4A',
                        fontSize: 11,
                        fontFamily: 'OpenSans-Light',
                    }}>Nilai Investasi</Text>
                    <Text style={{
                        color: '#4A4A4A',
                        fontSize: 14,
                        fontFamily: 'OpenSans-SemiBold',
                    }}>Rp {formatNumber(nilai)}</Text>
                </View>
                <View>
                    <Text style={{
                        color: '#4A4A4A',
                        fontSize: 11,
                        fontFamily: 'OpenSans-Light',
                    }}>Manfaat didapat</Text>
                    <Text style={{
                        color: '#FFAF16',
                        fontSize: 14,
                        fontFamily: 'OpenSans-SemiBold',
                    }}>Rp {formatNumber(manfaat)}</Text>
                </View>
            </View>
        </View>
    );
};

export default Produk;

const styles = StyleSheet.create({
    produk: {
        width: '100%',
        minHeight: 158,
        borderRadius: 6,
        backgroundColor: 'white',
        elevation: 1,
        paddingLeft: 18,
        paddingRight: 18,
        marginBottom: 20,
    },
    namaProduk: {
        display: 'flex',
        paddingTop: 20,
        flexDirection: 'row',
        marginBottom: 15,
    },
    detailProduk: {
        marginLeft: 15,
    },
    image: {
        width: 40,
        height: 40,
        borderRadius: 50,
    },
    hr: {
        borderBottomColor: '#DADADA',
        borderBottomWidth: 1,
        marginTop: 10,
    },
    title: {
        fontFamily: 'OpenSans-Bold',
        fontSize: 14,
        color: '#4A4A4A',
    },
    lihatDetail: {
        fontFamily: 'OpenSans-SemiBold',
        fontSize: 10,
        color: '#5683EC',
    },
    content: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 20,
    },
});
