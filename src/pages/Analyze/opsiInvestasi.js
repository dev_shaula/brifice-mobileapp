/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { StyleSheet, View, Text, RefreshControl, TouchableOpacity, ActivityIndicator, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { britama, deposito, dplk, reksadana } from '../../assets';
import { Header } from '../../components';
import { getDetailOpsiAnalyze, getOpsiAnalyze } from '../../redux/actions';
import formatNumber from '../../utils/formatNumber';

const OpsiInvestasi = ({ navigation }) => {
    const [refreshing, setRefreshing] = useState(false);
    const { result, opsiAnalyze, loading } = useSelector(state => state.analyze);
    const { token } = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    };

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        dispatch(getOpsiAnalyze(token, result.avg_pemasukkan, result.avg_pengeluaran, result.idle_money));
        wait(2000).then(() => setRefreshing(false));
    }, []);

    useEffect(() => {
        dispatch(getOpsiAnalyze(token, result.avg_pemasukkan, result.avg_pengeluaran, result.idle_money));
    }, []);

    return (
        <View>
            <Header haveArrow actionIcon={() => navigation.goBack(null)} headerTitle="Opsi Investasi" />
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
            >
                <View style={styles.container}>
                    <View style={styles.info}>
                        <Text style={styles.infoText}><Text style={{ fontFamily: 'OpenSans-Bold' }}>Informasi</Text> : Berikut adalah beberapa produk investasi rekomendasi dari kami yang sesuai dengan profile anda.</Text>
                    </View>

                    {
                        loading ? (
                            <ActivityIndicator size="large" color="#2736C1" style={{ marginTop: 10 }} />
                        ) : (
                            <>
                                {
                                    opsiAnalyze.map((val) => (
                                        <View style={styles.produk} key={val.id}>
                                            <View style={styles.namaProduk}>
                                                {
                                                    val.nama_produk === 'Britama Rencana' ? (
                                                        <Image source={britama} />
                                                    ) : (
                                                        val.nama_produk === 'Deposito' ? (
                                                            <Image source={deposito} />
                                                        ) : (
                                                            val.nama_produk === 'DPLK' ? (
                                                                <Image source={dplk} />
                                                            ) : (
                                                                <Image source={reksadana} />
                                                            )
                                                        )
                                                    )
                                                }
                                                <View style={styles.detailNama}>
                                                    <Text style={styles.produkTitle}>{val.nama_produk}</Text>
                                                </View>
                                            </View>
                                            <View style={styles.hr} />
                                            <Text style={styles.deskripsiProduk}>{val.deskripsi_produk}</Text>
                                            <View style={styles.hr} />
                                            <View style={styles.simulasi}>
                                                <Text style={styles.simulasiJudul}>
                                                    {
                                                        val.nama_produk === 'Britama Rencana' ? (
                                                            'Setoran rutin per bulan'
                                                        ) : (
                                                            val.nama_produk === 'Deposito' ? (
                                                                'Bunga + Saldo Deposito dengan jangka waktu:'
                                                            ) : (
                                                                'Setoran rutin'
                                                            )
                                                        )
                                                    }
                                                </Text>
                                                {
                                                    val.nama_produk !== 'Deposito' && (
                                                        <Text style={styles.simulasiTotal}>Rp {formatNumber(val.idle_money)}</Text>
                                                    )
                                                }
                                            </View>
                                            {
                                                val.nama_produk !== 'DPLK' ? (
                                                    <>
                                                        <View style={styles.hr} />
                                                        <View>
                                                            {
                                                                val.nama_produk === 'Deposito' ? (
                                                                    <>
                                                                        <View style={styles.simulasi}>
                                                                            <Text style={styles.simulasiJudul}>6 Bulan</Text>
                                                                            <Text style={styles.simulasiTotal}>Rp {formatNumber(val.total_1)}</Text>
                                                                        </View>
                                                                        <View style={styles.simulasi}>
                                                                            <Text style={styles.simulasiJudul}>12 Bulan</Text>
                                                                            <Text style={styles.simulasiTotal}>Rp {formatNumber(val.total_2)}</Text>
                                                                        </View>
                                                                        <View style={styles.simulasi}>
                                                                            <Text style={styles.simulasiJudul}>24 Bulan</Text>
                                                                            <Text style={styles.simulasiTotal}>Rp {formatNumber(val.total_3)}</Text>
                                                                        </View>
                                                                    </>
                                                                ) : (
                                                                    val.nama_produk === 'Britama Rencana' ? (
                                                                        <>
                                                                            <View style={styles.simulasi}>
                                                                                <Text style={styles.simulasiJudul}>Total Investasi 12 bulan</Text>
                                                                                <Text style={styles.simulasiTotal}>Rp {formatNumber(val.total_1)}</Text>
                                                                            </View>
                                                                            <View style={styles.simulasi}>
                                                                                <Text style={styles.simulasiJudul}>Total Investasi 60 bulan</Text>
                                                                                <Text style={styles.simulasiTotal}>Rp {formatNumber(val.total_2)}</Text>
                                                                            </View>
                                                                            <View style={styles.simulasi}>
                                                                                <Text style={styles.simulasiJudul}>Total Investasi 120 bulan</Text>
                                                                                <Text style={styles.simulasiTotal}>Rp {formatNumber(val.total_3)}</Text>
                                                                            </View>
                                                                        </>
                                                                    ) : (
                                                                        <>
                                                                            <View style={styles.simulasi}>
                                                                                <Text style={styles.simulasiJudul}>Imbal hasil + saldo 6 bulan</Text>
                                                                                <Text style={styles.simulasiTotal}>Rp {formatNumber(val.total_1)}</Text>
                                                                            </View>
                                                                            <View style={styles.simulasi}>
                                                                                <Text style={styles.simulasiJudul}>Imbal hasil + saldo 12 bulan</Text>
                                                                                <Text style={styles.simulasiTotal}>Rp {formatNumber(val.total_2)}</Text>
                                                                            </View>
                                                                            <View style={styles.simulasi}>
                                                                                <Text style={styles.simulasiJudul}>Imbal hasil + saldo 24 bulan</Text>
                                                                                <Text style={styles.simulasiTotal}>Rp {formatNumber(val.total_3)}</Text>
                                                                            </View>
                                                                        </>
                                                                    )
                                                                )
                                                            }
                                                        </View>
                                                    </>
                                                ) : (
                                                    <>
                                                        <View style={styles.hr} />
                                                        <View style={styles.simulasi}>
                                                            <Text style={styles.simulasiJudul}>Manfaat Pensiun Sebelum Pajak</Text>
                                                            <Text style={styles.simulasiTotal}>Rp {formatNumber(val.total_1)}</Text>
                                                        </View>
                                                    </>
                                                )
                                            }
                                            <TouchableOpacity onPress={() => {
                                                dispatch(getDetailOpsiAnalyze(token, val.id, result.idle_money));
                                                navigation.navigate('DetailOpsiInvestasi');
                                            }}>
                                                <Text style={styles.lihatDetil}>Lihat Detail</Text>
                                            </TouchableOpacity>
                                        </View>
                                    ))
                                }
                            </>
                        )
                    }
                </View>
            </ScrollView>
        </View>
    );
};

export default OpsiInvestasi;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 11,
        marginBottom: 40,
    },
    info: {
        width: '100%',
        minHeight: 50,
        backgroundColor: 'white',
        paddingVertical: 10,
        paddingLeft: 22,
        paddingRight: 17,
        borderRadius: 6,
    },
    infoText: {
        fontSize: 10,
        fontFamily: 'OpenSans-Light',
    },
    produk: {
        width: '100%',
        minHeight: 190,
        borderRadius: 6,
        backgroundColor: 'white',
        marginVertical: 11,
        marginBottom: 20,
    },
    namaProduk: {
        display: 'flex',
        flexDirection: 'row',
        paddingLeft: 20,
        paddingTop: 20,
        paddingBottom: 16,
    },
    detailNama: {
        paddingLeft: 16,
        paddingTop: 8,
    },
    produkTitle: {
        fontSize: 14,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    jenisProduk: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
    },
    hr: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#DADADA',
        width: '100%',
    },
    deskripsiProduk: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
        paddingVertical: 16,
        paddingHorizontal: 20,
    },
    simulasi: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 16,
    },
    simulasiJudul: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Reguler',
    },
    simulasiTotal: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    lihatDetil: {
        fontSize: 14,
        color: '#5683EC',
        fontFamily: 'OpenSans-Bold',
        textAlign: 'center',
        paddingVertical: 16,
    },
});
