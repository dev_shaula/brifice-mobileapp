/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, Text, View, RefreshControl, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { analyzeEmpty } from '../../assets';
import { Button, Header, Modal, Offline } from '../../components';
import { getAnalyze, getOpsiAnalyze, logout } from '../../redux/actions';
import { HasilAnalisa, Pemasukan, Pengeluaran } from './component';
import NetInfo from '@react-native-community/netinfo';

const Analyze = ({ navigation }) => {
    const dispatch = useDispatch();
    const { result, error } = useSelector(state => state.analyze);
    const { token } = useSelector(state => state.auth);
    const [refreshing, setRefreshing] = useState(false);
    const [loading, setLoading] = useState(true);
    const [connect, setConnect] = useState(true);
    const [openModal, setOpenModal] = useState(false);

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    };

    console.log(error);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        dispatch(getAnalyze(token));
        wait(2000).then(() => {
            setRefreshing(false);
        });
    }, []);

    useEffect(() => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
        }, 2000);
    }, []);

    useEffect(() => {
        if (error === 'Token is expired') {
            setOpenModal(true);
        } else {
            setOpenModal(false);
        }
    }, [error]);

    useEffect(() => {
        const unsubscribe = NetInfo.addEventListener(state => {
            setConnect(state.isConnected);
        });
        unsubscribe();
    });

    return (
        <>
            {
                loading ? (
                    <View style={styles.container}>
                        <ActivityIndicator color="#2736C1" size="large" />
                    </View>
                ) : (
                    result.idle_money === 0 || !connect ? (
                        <>
                            {
                                !connect && (
                                    <Offline />
                                )
                            }
                            <Header haveArrow actionIcon={() => navigation.navigate('MainApp')} />
                            <View style={styles.container}>
                                <Image source={analyzeEmpty} style={styles.image} />
                                <Text style={styles.title}>Analyze</Text>
                                <Text style={styles.desc}>Kami akan <Text style={{ fontFamily: 'OpenSans-Bold' }}>menganalisa pemasukan dan pengeluaran</Text> untuk menemukan Idle Money yang bisa kamu pakai investasi berdasarkan mutasi kamu selama 3 bulan terakhir.</Text>
                                <Button textBtn="Oke, Mengerti" onPress={() => navigation.navigate('MainApp')} />
                            </View>
                        </>
                    ) : (
                        <>
                            {
                                !connect && (
                                    <Offline />
                                )
                            }
                            <Header haveArrow actionIcon={() => navigation.navigate('MainApp')} headerTitle="Analyze" />
                            <ScrollView
                                refreshControl={
                                    <RefreshControl
                                        refreshing={refreshing}
                                        onRefresh={onRefresh}
                                    />
                                }
                            >
                                <View style={styles.content}>
                                    <Pemasukan navigation={navigation} />
                                    <Pengeluaran navigation={navigation} />
                                    <HasilAnalisa price={result.idle_money} />
                                    <View style={styles.btn}>
                                        <Button textBtn="Lihat Opsi Investasi" onPress={() => {
                                            dispatch(getOpsiAnalyze(token, result.avg_pemasukkan, result.avg_pengeluaran, result.idle_money, (status, detail) => {
                                                if (status === 'success') {
                                                    navigation.navigate('OpsiInvestasi');
                                                } else {
                                                    dispatch({ type: 'QUESTIONS_BEGIN' });
                                                    navigation.navigate('Questions');
                                                }
                                            }));
                                        }} />
                                    </View>
                                </View>
                                {
                                    openModal && (
                                        <Modal
                                            visible={openModal}
                                            onClose={() => setOpenModal(false)}
                                            title="Sesi Anda telah berakhir, silahkan login kembali"
                                            okText="Ya"
                                            onOk={() => {
                                                setOpenModal(false);
                                                dispatch(logout());
                                                navigation.replace('Login');
                                            }}
                                        />
                                    )
                                }
                            </ScrollView>
                        </>
                    )
                )
            }
        </>
    );
};

export default Analyze;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        flex: 1,
    },
    image: {
        width: 224,
        height: 210,
        resizeMode: 'cover',
    },
    title: {
        fontSize: 20,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
        paddingTop: 105,
    },
    desc: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
        paddingTop: 16,
        textAlign: 'center',
        paddingBottom: 65,
        paddingHorizontal: 54,
    },
    content: {
        paddingHorizontal: 20,
        paddingVertical: 16,
    },
    btn: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 16,
    },
});
