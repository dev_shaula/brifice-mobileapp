/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Pie from 'react-native-pie';
import { useDispatch, useSelector } from 'react-redux';
import { Box, RataRata } from '../../../../components';
import { detailParameter } from '../../../../redux/actions';

const Pengeluaran = (props) => {
    const dispatch = useDispatch();
    const { result } = useSelector(state => state.analyze);
    const [pengeluaran, setPengeluaran] = useState([]);
    const [analisis, setAnalisis] = useState([]);
    const color = ['#2736C1', '#FFB036', '#9F32A2', '#329F8C', '#E49B2E', '#2EB9E4', '#E42E2E', '#389F5B', '#246960', '#DDDDDD', '#A11C20', '#F4A52C', '#15977F', '#B7FF8A', '#599bc5', '#2ced56', '#053810', '#ebb51e', '#eb1eb8', '#361eeb'];

    // const generateRandomColor = () => {
    //     const randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
    //     return randomColor;
    // };

    const edit = () => {
        dispatch(detailParameter({ analisis: 'keluar' }));
        props.navigation.navigate('EditParameter');
    };

    useEffect(() => {
        if (result.detail.keluar !== null) {
            for (let i = 0; i < color.length; i++) {
                const resultAnalisis = result.detail.keluar.map((val, idx) => ({
                    percentage: val.persen,
                    color: color[idx],
                    remarks: val.remarks,
                }));
                const resultPengeluaran = resultAnalisis.map(val => ({
                    percentage: val.percentage,
                    color: val.color,
                }));
                setAnalisis([...resultAnalisis]);
                setPengeluaran([...resultPengeluaran]);
            }
        }
    }, [result.detail.keluar]);

    return (
        <View style={styles.pengeluaran}>
            <Text style={styles.title}>Analisa Pengeluaran</Text>
            <Text style={styles.desc}>Berikut adalah rata-rata pengeluaran kamu. Untuk melihat detil atau melakukan penyesuaian, pilih "Lihat Detil atau Sesuaikan".</Text>
            <View style={styles.pie}>
                <Pie
                    radius={80}
                    innerRadius={50}
                    sections={pengeluaran}
                    strokeCap={'butt'}
                />
            </View>
            {
                analisis.map((val, idx) => (
                    <Box color={val.color} text={val.remarks} key={idx} />
                ))
            }
            <View style={{ marginTop: 36 }}>
                <RataRata title="Rata Rata pengeluaran" price={result.avg_pengeluaran} />
            </View>
            <TouchableOpacity onPress={edit}>
                <Text style={styles.lihatDetil}>Lihat Detil atau Sesuaikan</Text>
            </TouchableOpacity>
        </View>
    );
};

export default Pengeluaran;

const styles = StyleSheet.create({
    pengeluaran: {
        width: '100%',
        minHeight: 390,
        borderRadius: 15,
        backgroundColor: 'white',
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        marginTop: 16,
    },
    title: {
        fontSize: 14,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    desc: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Regular',
        paddingTop: 6,
    },
    pie: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: 48,
        alignItems: 'center',
    },
    lihatDetil: {
        fontSize: 14,
        color: '#5683EC',
        fontFamily: 'OpenSans-Bold',
        textAlign: 'center',
        paddingVertical: 16,
    },
});
