import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { RataRata } from '../../../../components';

const HasilAnalisa = ({ price }) => {
    return (
        <View style={styles.analisa}>
            <Text style={styles.title}>Hasil Analisa</Text>
            <Text style={styles.desc}>Berdasarkan analisa pemasukan dan pengeluaran kamu, maka rata-rata Idle Money kamu sejumlah:</Text>
            <View style={{ marginTop: 24 }}>
                <RataRata title="Idle Money:" price={price} />
            </View>
        </View>
    );
};

export default HasilAnalisa;

const styles = StyleSheet.create({
    analisa: {
        width: '100%',
        minHeight: 183,
        borderRadius: 15,
        backgroundColor: 'white',
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        marginTop: 16,
    },
    title: {
        fontSize: 14,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    desc: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Regular',
        paddingTop: 6,
    },
});
