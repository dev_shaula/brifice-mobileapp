/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, StyleSheet, Text, View, RefreshControl, Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Checkbox, Header } from '../../components';
import { ScrollView } from 'react-native-gesture-handler';
import { editParameter, getAnalyzeAdjust } from '../../redux/actions';

const EditParameter = ({ navigation }) => {
    const { analisis, adjustKeluar, adjustMasuk, loadingAdjust } = useSelector(state => state.analyze);
    const { token } = useSelector(state => state.auth);
    const [parameter, setParameter] = useState([]);
    const dispatch = useDispatch();
    const [refreshing, setRefreshing] = useState(false);

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    };

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        dispatch(getAnalyzeAdjust(token, analisis));
        wait(2000).then(() => {
            setRefreshing(false);
        });
    }, []);

    const edit = () => {
        const findStatusTrue = parameter.filter(val => val.status !== false);
        const result = findStatusTrue.map(value => ({
            remarks: value.remarks,
            nominal: value.nominal,
            total: value.total,
        }));
        dispatch(editParameter(token, result, analisis, navigation.navigate('Analyze')));
    };

    useEffect(() => {
        if (adjustMasuk || adjustKeluar) {
            if (adjustMasuk.length > 0) {
                const param = adjustMasuk.map((val, idx) => ({
                    id: Number(idx),
                    status: val.total >= 3 ? true : false,
                    remarks: val.remarks,
                    nominal: val.nominal,
                    total: val.total,
                }));
                setParameter([...param]);
            } else {
                const param = adjustKeluar.map((val, idx) => ({
                    id: Number(idx),
                    status: val.total >= 3 ? true : false,
                    remarks: val.remarks,
                    nominal: val.nominal,
                    total: val.total,
                }));
                setParameter([...param]);
            }
        }
    }, [adjustKeluar, adjustMasuk]);

    useEffect(() => {
        dispatch(getAnalyzeAdjust(token, analisis));
    }, []);

    return (
        <View>
            <Header haveArrow actionIcon={() => navigation.navigate('Analyze')} headerTitle="Detil atau Sesuaikan" />
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
            >
                <View style={styles.container(loadingAdjust)}>
                    {
                        loadingAdjust ? (
                            <View style={styles.loading}>
                                <ActivityIndicator color="#2736C1" size="large" />
                            </View>
                        ) : (
                            <>
                                <View style={styles.containerCheckbox}>
                                    <Text style={styles.title}>Sesuaikan item {analisis === 'masuk' ? 'Pemasukan' : 'Pengeluaran'}</Text>
                                    <Text style={styles.desc}>Berikut adalah parameter {analisis === 'masuk' ? 'Pemasukan' : 'Pengeluaran'} rutin anda.</Text>
                                    <View style={{ marginTop: 41 }}>
                                        {
                                            parameter.map((val, idx) => (
                                                <Checkbox
                                                    selected={val.status}
                                                    text={val.remarks}
                                                    onPress={() => {
                                                        const find = parameter.findIndex(obj => obj.id === val.id);
                                                        const updateObj = { ...parameter[find], status: !val.status };
                                                        const updateParam = [
                                                            ...parameter.slice(0, find),
                                                            updateObj,
                                                            ...parameter.slice(find + 1),
                                                        ];
                                                        setParameter(updateParam);
                                                    }}
                                                    key={idx}
                                                />
                                            ))
                                        }
                                    </View>
                                </View>
                                <View style={styles.btn}>
                                    <Button textBtn="Simpan" onPress={edit} />
                                </View>
                            </>
                        )
                    }
                </View>
            </ScrollView>
        </View>
    );
};

export default EditParameter;

const styles = StyleSheet.create({
    container: (loadingAdjust) => (
        {
            minHeight: Dimensions.get('window').height,
            backgroundColor: 'white',
        }
    ),
    containerCheckbox: {
        paddingTop: 40,
        paddingLeft: 33,
    },
    title: {
        fontSize: 14,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    desc: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Regular',
    },
    btn: {
        marginTop: 10,
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        bottom: 55,
    },
    checkbox: {
        display: 'flex',
        flexDirection: 'row',
        paddingBottom: 28,
    },
    loading: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        flex: 1,
    },
});
