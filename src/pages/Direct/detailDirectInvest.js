import React, { useEffect, useState } from 'react';
import { ActivityIndicator, StyleSheet, Text, View, Dimensions, Pressable, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Header, Modal } from '../../components';
import Ionicons from 'react-native-vector-icons/Ionicons';
import formatNumber from '../../utils/formatNumber';
import CurrencyInput from 'react-native-currency-input';
import { getDetailDirect } from '../../redux/actions';
import { britama, deposito, dplk, reksadana } from '../../assets';

const DetailDirectInvest = ({ navigation }) => {
    const { detailDirect, loading } = useSelector(state => state.direct);
    const [wantChange, setWantChange] = useState(false);
    const [openModal, setOpenModal] = useState(false);
    const [edit, setEdit] = useState(detailDirect.idle_money);
    const [ajukanInvest, setAjukanInvest] = useState(false);
    const dispatch = useDispatch();
    const { token } = useSelector(state => state.auth);

    useEffect(() => {
        if (edit === 0) {
            setEdit(detailDirect.idle_money);
        }
    }, [detailDirect.idle_money, edit]);

    return (
        <View>
            <Header haveArrow actionIcon={() => navigation.navigate('Direct')} headerTitle={detailDirect.nama_produk} />
            <ScrollView>
                <View>
                    {
                        loading ? (
                            <View style={styles.loading}>
                                <ActivityIndicator size="large" color="#2736C1" style={{ marginTop: 10 }} />
                            </View>
                        ) : (
                            <View style={styles.container}>
                                <View style={styles.produk}>
                                    <View style={styles.namaProduk}>
                                        {
                                            detailDirect.nama_produk === 'Britama Rencana' ? (
                                                <Image source={britama} />
                                            ) : (
                                                detailDirect.nama_produk === 'Deposito' ? (
                                                    <Image source={deposito} />
                                                ) : (
                                                    detailDirect.nama_produk === 'DPLK' ? (
                                                        <Image source={dplk} />
                                                    ) : (
                                                        <Image source={reksadana} />
                                                    )
                                                )
                                            )
                                        }
                                        <View style={styles.detailNama}>
                                            <Text style={styles.produkTitle}>{detailDirect.nama_produk}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.hr} />
                                    <Text style={styles.deskripsiProduk}>{detailDirect.deskripsi_produk}</Text>
                                </View>
                                <View style={styles.simulasi}>
                                    <Text style={{ fontSize: 11, color: '#4A4A4A', fontFamily: 'OpenSans-Regular', paddingLeft: 20, paddingTop: 24, paddingBottom: 12 }}>Maka anda akan mendapatkan manfaat sebesar :</Text>
                                    {
                                        detailDirect.nama_produk !== 'DPLK' ? (
                                            <>
                                                {
                                                    detailDirect.nama_produk === 'Deposito' ? (
                                                        <>
                                                            <View style={styles.simulasiTotal}>
                                                                <Text style={styles.simulasiPrice}>Rp {formatNumber(detailDirect.total_1)}</Text>
                                                                <Text style={styles.simulasiJudul}>dalam 6 bulan</Text>
                                                            </View>
                                                            <View style={styles.simulasiTotal}>
                                                                <Text style={styles.simulasiPrice}>Rp {formatNumber(detailDirect.total_2)}</Text>
                                                                <Text style={styles.simulasiJudul}>dalam 12 bulan</Text>
                                                            </View>
                                                            <View style={[styles.simulasiTotal, { marginBottom: 20 }]}>
                                                                <Text style={styles.simulasiPrice}>Rp {formatNumber(detailDirect.total_3)}</Text>
                                                                <Text style={styles.simulasiJudul}>dalam 24 bulan</Text>
                                                            </View>
                                                        </>
                                                    ) : (
                                                        detailDirect.nama_produk === 'Britama Rencana' ? (
                                                            <>
                                                                <View style={styles.simulasiTotal}>
                                                                    <Text style={styles.simulasiPrice}>Rp {formatNumber(detailDirect.total_1)}</Text>
                                                                    <Text style={styles.simulasiJudul}>Total Investasi 12 bulan</Text>
                                                                </View>
                                                                <View style={styles.simulasiTotal}>
                                                                    <Text style={styles.simulasiPrice}>Rp {formatNumber(detailDirect.total_2)}</Text>
                                                                    <Text style={styles.simulasiJudul}>Total Investasi 60 bulan</Text>
                                                                </View>
                                                                <View style={[styles.simulasiTotal, { marginBottom: 20 }]}>
                                                                    <Text style={styles.simulasiPrice}>Rp {formatNumber(detailDirect.total_3)}</Text>
                                                                    <Text style={styles.simulasiJudul}>Total Investasi 120 bulan</Text>
                                                                </View>
                                                            </>
                                                        ) : (
                                                            <>
                                                                <View style={styles.simulasiTotal}>
                                                                    <Text style={styles.simulasiPrice}>Rp {formatNumber(detailDirect.total_1)}</Text>
                                                                    <Text style={styles.simulasiJudul}>Imbal hasil + saldo 6 bulan</Text>
                                                                </View>
                                                                <View style={styles.simulasiTotal}>
                                                                    <Text style={styles.simulasiPrice}>Rp {formatNumber(detailDirect.total_2)}</Text>
                                                                    <Text style={styles.simulasiJudul}>Imbal hasil + saldo 12 bulan</Text>
                                                                </View>
                                                                <View style={[styles.simulasiTotal, { marginBottom: 20 }]}>
                                                                    <Text style={styles.simulasiPrice}>Rp {formatNumber(detailDirect.total_3)}</Text>
                                                                    <Text style={styles.simulasiJudul}>Imbal hasil + saldo 24 bulan</Text>
                                                                </View>
                                                            </>
                                                        )
                                                    )
                                                }
                                            </>
                                        ) : (
                                            <>
                                                <View style={[styles.simulasiTotal, { marginBottom: 20 }]}>
                                                    <Text style={styles.simulasiPrice}>Rp {formatNumber(detailDirect.total_1)}</Text>
                                                    <Text style={styles.simulasiJudul}>Manfaat Pensiun Sebelum Pajak</Text>
                                                </View>
                                            </>
                                        )
                                    }
                                </View>
                                <View style={[styles.simulasi, { minHeight: 245 }]}>
                                    <View style={styles.simulasiContent}>
                                        <Text style={styles.simulasiTitle}>Ajukan Investasi</Text>
                                        <Text style={[styles.simulasiDesc, { paddingTop: 0 }]}>Ayo mulai berinvestasi rutin sekarang!</Text>
                                    </View>
                                    <Text style={{ fontSize: 11, color: '#4A4A4A', fontFamily: 'OpenSans-Regular', paddingLeft: 20, paddingTop: 24, paddingBottom: 12 }}>Rekomendasi Jumlah Investasi</Text>
                                    <View style={{ paddingLeft: 12, paddingRight: 12 }}>
                                        <View style={wantChange ? styles.editTotalInvestasi : styles.totalInvestasi}>
                                            {
                                                !wantChange ? (
                                                    <>
                                                        <Text style={styles.total}>Rp {formatNumber(detailDirect.idle_money)}</Text>
                                                        <Pressable onPress={() => setOpenModal(true)}>
                                                            <Text style={{ paddingRight: 13, fontSize: 14, color: '#5683EC', fontFamily: 'OpenSans-SemiBold' }}>Ubah</Text>
                                                        </Pressable>
                                                    </>
                                                ) : (
                                                    <View style={{ display: 'flex', flexDirection: 'row', width: '100%', justifyContent: 'space-between' }}>
                                                        <CurrencyInput
                                                            value={edit}
                                                            onChangeValue={setEdit}
                                                            prefix="Rp "
                                                            delimiter="."
                                                            separator="."
                                                            precision={0}
                                                            style={styles.inputNum}
                                                        />
                                                        <Button width={50} height={40} textBtn="Ubah" onPress={() => {
                                                            console.log('edit total', edit);
                                                            dispatch(getDetailDirect(token, detailDirect.id, edit));
                                                        }} />
                                                    </View>
                                                )
                                            }
                                        </View>
                                    </View>
                                    <View style={styles.btn}>
                                        <Button width={295} textBtn="Ajukan Investasi" onPress={() => setAjukanInvest(true)} />
                                    </View>
                                </View>
                                <View style={[styles.simulasi, { minHeight: 86, marginBottom: 19 }]}>
                                    <View style={styles.simulasiContent}>
                                        <Text style={styles.simulasiTitle}>Butuh Bantuan?</Text>
                                        <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row' }}>
                                            <Text style={[styles.simulasiDesc, { paddingTop: 8 }]}>Hubungi kami jika anda butuh <Text style={{ fontFamily: 'OpenSans-Bold', color: '#5683EC' }}>konsultasi lanjutan</Text></Text>
                                            <Ionicons name="chevron-forward-outline" size={30} color="#5683EC" />
                                        </View>
                                    </View>
                                </View>
                            </View>
                        )
                    }
                </View>
            </ScrollView>
            {
                openModal && (
                    <Modal
                        visible={openModal}
                        onClose={() => setOpenModal(false)}
                        title="Ubah Nominal?"
                        desc="Apakah anda yakin ingin mengubah jumlah nominal yang kami rekomendasikan?"
                        okText="Ya"
                        cancelText="Tidak"
                        onOk={() => {
                            setOpenModal(false);
                            setWantChange(true);
                        }}
                    />
                )
            }
            {
                ajukanInvest && (
                    <Modal
                        visible={ajukanInvest}
                        onClose={() => setAjukanInvest(false)}
                        title="Ajukan Investasi?"
                        desc={`Apakah anda yakin ingin mengajukan investasi dengan nominal Rp ${formatNumber(edit)}?`}
                        okText="Ya"
                        cancelText="Tidak"
                        onOk={() => {
                            dispatch({
                                type: 'DETAIL_DIRECT_INVEST_SUCCESS',
                                loading: false,
                                detailDirect: {
                                    id: 0,
                                    nama_produk: '',
                                    jenis_produk: '',
                                    deskripsi_produk: '',
                                    profil_resiko: 0,
                                    idle_money: 0,
                                    total_1: 0,
                                    total_15: 0,
                                    total_20: 0,
                                    total_30: 0,
                                },
                            });
                            setEdit(0);
                            setWantChange(false);
                            setAjukanInvest(false);
                            navigation.replace('SuccessPengajuan');
                        }}
                    />
                )
            }
        </View>
    );
};

export default DetailDirectInvest;

const styles = StyleSheet.create({
    container: {
        minHeight: Dimensions.get('window').height,
        paddingLeft: 20,
        paddingRight: 20,
        paddingVertical: 11,
        marginBottom: 40,
    },
    produk: {
        width: '100%',
        minHeight: 160,
        borderRadius: 6,
        backgroundColor: 'white',
        marginVertical: 11,
    },
    namaProduk: {
        display: 'flex',
        flexDirection: 'row',
        paddingLeft: 20,
        paddingTop: 20,
        paddingBottom: 16,
    },
    detailNama: {
        paddingLeft: 16,
        paddingTop: 8,
    },
    produkTitle: {
        fontSize: 14,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    jenisProduk: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
    },
    hr: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#DADADA',
        width: 295,
        marginHorizontal: 10,
    },
    deskripsiProduk: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
        paddingVertical: 16,
        paddingHorizontal: 20,
    },
    simulasi: {
        width: '100%',
        minHeight: 50,
        borderRadius: 6,
        backgroundColor: 'white',
        marginVertical: 11,
    },
    simulasiContent: {
        paddingHorizontal: 20,
        paddingVertical: 20,
    },
    simulasiTitle: {
        fontSize: 14,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    simulasiDesc: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
        paddingTop: 24,
    },
    totalInvestasi: {
        width: '100%',
        height: 42,
        borderRadius: 6,
        backgroundColor: '#F9F9F9',
        paddingVertical: 12,
        paddingLeft: 12,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    editTotalInvestasi: {
        width: '100%',
        borderRadius: 6,
    },
    total: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    simulasiTotal: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 16,
    },
    simulasiJudul: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Regular',
    },
    simulasiPrice: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    btn: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 16,
        marginBottom: 16,
    },
    loading: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        minHeight: Dimensions.get('window').height,
    },
    inputNum: {
        width: 250,
        height: 42,
        borderRadius: 6,
        backgroundColor: '#F9F9F9',
        padding: 10,
    },
});
