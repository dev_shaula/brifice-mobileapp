/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { ActivityIndicator, Image, RefreshControl, StyleSheet, Text, TouchableOpacity, View, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { britama, deposito, dplk, investEmpty, reksadana } from '../../assets';
import { Button, Header, Modal, Offline } from '../../components';
import { getDetailDirectInvest, getDirectInvest, logout } from '../../redux/actions';
import NetInfo from '@react-native-community/netinfo';

const Direct = ({ navigation }) => {
    const dispatch = useDispatch();
    const { token } = useSelector(state => state.auth);
    const { direct, loading, error } = useSelector(state => state.direct);
    const [refreshing, setRefreshing] = useState(false);
    const [connect, setConnect] = useState(true);
    const [openModal, setOpenModal] = useState(false);

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    };

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        dispatch(getDirectInvest(token));
        wait(2000).then(() => setRefreshing(false));
    }, []);

    useEffect(() => {
        dispatch(getDirectInvest(token));
    }, []);

    useEffect(() => {
        const unsubscribe = NetInfo.addEventListener(state => {
            setConnect(state.isConnected);
        });
        unsubscribe();
    });

    console.log(error);

    useEffect(() => {
        if (error === 'Token is expired') {
            setOpenModal(true);
        } else {
            setOpenModal(false);
        }
    }, [error]);

    return (
        <>
            {
                loading ? (
                    <View style={styles.loading}>
                        <ActivityIndicator size="large" color="#2736C1" style={{ marginTop: 10 }} />
                    </View>
                ) : (
                    direct.length <= 0 || !connect ? (
                        <>
                            {
                                !connect && (
                                    <Offline />
                                )
                            }
                            <Header haveArrow actionIcon={() => navigation.navigate('MainApp')} />
                            <View style={styles.container}>
                                <Image source={investEmpty} style={styles.image} />
                                <Text style={styles.title}>Direct Invest</Text>
                                <Text style={styles.desc}>Anda dapat memilih produk investasi yang cocok, dan dapat membelinya langsung sesuai kemampuan anda.</Text>
                                <Button style={styles.btn} textBtn="Oke, Mengerti" onPress={() => navigation.navigate('MainApp')} />
                            </View>
                        </>
                    ) : (
                        <>
                            <Header haveArrow actionIcon={() => navigation.navigate('MainApp')} headerTitle="Produk Investasi" />
                            <ScrollView
                                refreshControl={
                                    <RefreshControl
                                        refreshing={refreshing}
                                        onRefresh={onRefresh}
                                    />
                                }
                            >
                                <View style={styles.containerProduk}>
                                    {
                                        direct.map((val) => (
                                            <View style={styles.produk} key={val.id}>
                                                <View style={styles.namaProduk}>
                                                    {
                                                        val.nama_produk === 'Britama Rencana' ? (
                                                            <Image source={britama} />
                                                        ) : (
                                                            val.nama_produk === 'Deposito' ? (
                                                                <Image source={deposito} />
                                                            ) : (
                                                                val.nama_produk === 'DPLK' ? (
                                                                    <Image source={dplk} />
                                                                ) : (
                                                                    <Image source={reksadana} />
                                                                )
                                                            )
                                                        )
                                                    }
                                                    <View style={styles.detailNama}>
                                                        <Text style={styles.produkTitle}>{val.nama_produk}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles.hr} />
                                                <Text style={styles.deskripsiProduk}>{val.deskripsi_produk}</Text>
                                                <View style={styles.hr} />
                                                <TouchableOpacity onPress={() => {
                                                    dispatch(getDetailDirectInvest(token, val.id));
                                                    navigation.navigate('DetailDirectInvest');
                                                }}>
                                                    <Text style={styles.lihatDetil}>Lihat Detail</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ))
                                    }
                                </View>
                                {
                                    openModal && (
                                        <Modal
                                            visible={openModal}
                                            onClose={() => setOpenModal(false)}
                                            title="Sesi Anda telah berakhir, silahkan login kembali"
                                            okText="Ya"
                                            onOk={() => {
                                                setOpenModal(false);
                                                dispatch(logout());
                                                navigation.replace('Login');
                                            }}
                                        />
                                    )
                                }
                            </ScrollView>
                        </>
                    )
                )
            }
        </>
    );
};

export default Direct;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        flex: 1,
    },
    containerProduk: {
        paddingHorizontal: 20,
        paddingVertical: 11,
        marginBottom: 40,
        flex: 1,
    },
    image: {
        width: 224,
        height: 210,
        resizeMode: 'cover',
    },
    title: {
        fontSize: 20,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
        paddingTop: 105,
    },
    desc: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
        paddingTop: 16,
        textAlign: 'center',
        paddingBottom: 65,
        paddingHorizontal: 54,
    },
    produk: {
        width: '100%',
        minHeight: 200,
        borderRadius: 6,
        backgroundColor: 'white',
        marginVertical: 11,
    },
    namaProduk: {
        display: 'flex',
        flexDirection: 'row',
        paddingLeft: 20,
        paddingTop: 20,
        paddingBottom: 16,
    },
    detailNama: {
        paddingLeft: 16,
        paddingTop: 8,
    },
    produkTitle: {
        fontSize: 14,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    jenisProduk: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
    },
    hr: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#DADADA',
        width: 295,
        marginHorizontal: 10,
    },
    deskripsiProduk: {
        fontSize: 11,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Light',
        paddingVertical: 16,
        paddingHorizontal: 20,
    },
    simulasi: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 16,
    },
    simulasiJudul: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Reguler',
    },
    simulasiTotal: {
        fontSize: 12,
        color: '#4A4A4A',
        fontFamily: 'OpenSans-Bold',
    },
    lihatDetil: {
        fontSize: 14,
        color: '#5683EC',
        fontFamily: 'OpenSans-Bold',
        textAlign: 'center',
        paddingVertical: 16,
    },
    loading: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        minHeight: Dimensions.get('window').height,
    },
});
