import React, { useEffect } from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';
import { ProgressBar } from '@react-native-community/progress-bar-android';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Splash = ({ navigation }) => {

    useEffect(() => {
        setTimeout(async () => {
            try {
                const hasToken = await AsyncStorage.getItem('token');
                hasToken !== null ? navigation.replace('MainApp') : navigation.replace('Login');
            } catch (error) {
                console.log(error);
            }
        }, 3000);
    }, [navigation]);
    return (
        <View style={styles.container}>
            <Image style={styles.img} source={require('../../assets/images/logo.png')} />
            <Text style={styles.briText}>BRI<Text style={{ color: '#FF9D07' }}>FICE</Text></Text>
            <Text style={styles.descText}>BRI Financial Advice</Text>
            <View style={styles.version}>
                <ProgressBar
                    styleAttr="Horizontal"
                    color="#2736C1"
                    style={styles.progress}
                />
                <Text style={{ textAlign: 'center' }}>Version 0.0.0.1</Text>
            </View>
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    img: {
        width: 200,
        height: 215,
    },
    briText: {
        fontSize: 40,
        fontWeight: 'bold',
        paddingTop: 10,
        color: '#2736C1',
        fontFamily: 'OpenSans-Bold',
    },
    descText: {
        fontSize: 13,
        color: '#4A4A4A',
        lineHeight: 18,
        fontFamily: 'OpenSans-Light',
    },
    version: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 10,
    },
    progress: {
        width: 190,
    },
});
